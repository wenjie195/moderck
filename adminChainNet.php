<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';


require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($presetUid),"s");
// $userData = $userDetails[0];

// $userChainNet = getReferralHistory($conn, " ORDER BY current_level DESC ");
$userChainNet = getReferralHistory($conn);
// $userLevel = $userRHDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Chain Net | MODERCK" />
<title>Chain Net | MODERCK</title>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Chain Net</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">

        <!-- <div class="text-center middle-div-width">
          <a href="#"><div  class="clean white-button ow-red-bg white-text smaller-button">Add</div></a> 
        </div>  -->

        <div class="width100 overflow-x">
            <table class="width100 gold-table ow-text-left-table">
                <thead>
                    <tr>
                        <!-- <th>S/N</th> -->
                        <th>LINK</th>
                        <th>REFERRAL</th>
                        <th>USERNAME</th>
                        <th>RANK</th>

                        <th>PV</th>
                        <th>GPV</th>

                        <th>STATUS</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                        if($userChainNet)
                        {
                            for($cnt = 0;$cnt < count($userChainNet) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo $userChainNet[$cnt]->getCurrentLevel();?></td>

                                <td>
                                  <?php
                                    $conn = connDB();
                                    $uplineUid = $userChainNet[$cnt]->getReferrerId();
                                    $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                                    echo $uplineDetails[0]->getUsername();
                                    $conn->close();
                                  ?>
                                </td>

                                <td>
                                  <?php
                                    $conn = connDB();
                                    $userUid = $userChainNet[$cnt]->getReferralId();
                                    $userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
                                    echo $userDetails[0]->getUsername();
                                    $userRank = $userDetails[0]->getRank();
                                    $userSalesComms = $userDetails[0]->getSalesCommission();
                                    $conn->close();
                                  ?>
                                </td>

                                <td><?php echo $userRank;?></td>
                                <td><?php echo $userSalesComms;?></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                            }
                        }
                    ?>   
                </tbody>
            </table>
        </div>


    </div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>