<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/" />
<link rel="canonical" href="https://agentpnchc.com/" /> -->
<meta property="og:title" content="REGISTER | MODERCK" />
<title>REGISTER | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding  min-height100 padding-top login-bg padding-top-bottom">

    <h1 class="h1 red-text text-center login-h1"><br><b>Register New User</b></h1><?php include 'header.php'; ?>

    <div class="login-div margin-auto">
        <form action="utilities/adminRegisterNewUserFunction.php" method="POST">

            <?php
            if(isset($_POST['user_uid']))
            {
            $conn = connDB();
            $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
            ?>

                <div class="fake-input-div">
                    <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Username" id="username" name="username" onChange="javascript:this.value=this.value.toLowerCase();" required>
                </div>

                <div class="clear"></div>

                <div class="fake-input-div">
                    <input type="email" class="input-css clean icon-input dark-tur-text2" placeholder="Email" id="email" name="email" required>
                </div>

                <!-- <div class="fake-input-div before-forgot"> -->
                <div class="fake-input-div">
                    <!-- <img src="img/password.png" class="input-icon"> -->
                    <input type="password" class="input-css clean password-input dark-tur-text2" placeholder="Password" id="password" name="password" required>
                    <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionA()" alt="Password" title="Password">
                </div>

                <div class="clear"></div>

                <div class="fake-input-div">
                    <input type="password" class="input-css clean password-input dark-tur-text2" placeholder="Confirm Password" id="retype_password" name="retype_password" required>
                    <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionB()" alt="Password" title="Password">
                </div>

                <div class="clear"></div>

                <div class="fake-input-div">
                    <input type="text" class="input-css clean icon-input dark-tur-text2" value="<?php echo $userDetails[0]->getUsername();?>" readonly>
                </div>

                <input class="input-css clean icon-input dark-tur-text2" type="hidden" value="<?php echo $_POST['user_uid'];?>" id="upline_uid" name="upline_uid" readonly>

                <button class="clean white-button ow-red-bg white-text" name="submit">Register</button>

            <?php
            }
            ?>
        
        </form>

        <div class="clear"></div>

    </div>

</div>

<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>