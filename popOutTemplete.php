<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$orderDetails = getOrders($conn, " WHERE status = 'PENDING' AND payment_references is not null ORDER BY date_created DESC");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminViewWithdrawal.php" />
    <meta property="og:title" content="Withdrawal Request | Samofa 莎魔髪" />
    <title>Order Checker | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminViewWithdrawal.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo AVB_ORDER_CHECKER ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo AVB_ID ?></th>
                    <th><?php echo _MAINJS_INDEX_USERNAME ?></th>
                    <th><?php echo AVB_AMOUNT ?></th>
                    <th><?php echo AVB_BILLID ?></th>
                    <th><?php echo AVB_DATE ?></th>
                    <th><?php echo AVB_RECEIPT ?></th>
                    <th><?php echo _ADMIN_ACTION ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($orderDetails)
                {
                    for($cnt = 0;$cnt < count($orderDetails) ;$cnt++)
                    {
                        $userDetails = getUser($conn, "WHERE uid=?",array("uid"),array($orderDetails[$cnt]->getUid()), "s");
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo '#'.$orderDetails[$cnt]->getId();?></td>
                            <td><?php echo $userDetails[0]->getUsername();?></td>
                            <td><?php echo number_format($orderDetails[$cnt]->getTotal(),2);?></td>
                            <td><?php echo $orderDetails[$cnt]->getPaymentReferences();?></td>
                            <td><?php echo date('d/m/Y',strtotime($orderDetails[$cnt]->getDateCreated()));?></td>
                            <td><a target="_blank" href="https://www.billplz.com/bills/<?= $orderDetails[$cnt]->getPaymentReferences() ?>" class="btn btn-xs"><i class="fa fa-file-text"></i></a></td>
                            <td>
                                <form id="action-form-<?= $cnt+1 ?>" action="cronjob/billPlzChecker.php" method="post">
                                    <input type="hidden" name="bill_id" value="<?= $orderDetails[$cnt]->getPaymentReferences() ?>">
                                    <input type="hidden" name="paid_amount" value="<?= $orderDetails[$cnt]->getTotal() ?>">
                                    <input type="hidden" name="uid" value="<?= $orderDetails[$cnt]->getUid() ?>">
                                    <input type="hidden" name="order_id" value="<?= $orderDetails[$cnt]->getId() ?>">
                                    <button type="button" name="delete" onclick="deleteOrder(<?= $cnt+1 ?>)" class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button>
                                    <button type="button" name="check" onclick="checkOrder(<?= $cnt+1 ?>)" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                </form>                
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?> 
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
<style>
.btn{
    cursor: pointer;
}
.btn-xs {
    padding: 1px 5px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.btn-success:hover {
    color: #fff;
    background-color: #449d44;
    border-color: #398439;
}
.btn-danger {
    color: #fff;
    background-color: #d9534f;
    border-color: #d43f3a;
}
.btn-danger:hover {
    color: #fff;
    background-color: #c9302c;
    border-color: #ac2925;
}
</style>
</body>
</html>
<script>
function checkOrder(no){
    swal({
        title: "Are you sure?",
        text: "Once approve, you will not be able to recover this Order!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
        var x = $("#action-form-"+no);
        x.find('.btn-success').attr('type','submit');
        x.find('.btn-success').attr('onclick','');
        x.find('.btn-success').click();
        }
    });
}
function deleteOrder(no){
    swal({
        title: "Are you sure?",
        text: "Once delete, you will not be able to recover this Order!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
        var x = $("#action-form-"+no);
        x.find('.btn-danger').attr('type','submit');
        x.find('.btn-danger').attr('onclick','');
        x.find('.btn-danger').click();
        }
    });
}
</script>