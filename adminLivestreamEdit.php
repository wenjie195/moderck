<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Livestream.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Edit Livestream | MODERCK" />
<title>Edit Livestream | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Edit Livestream</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">

    <div class="width100 inner-bg inner-padding">
        <div class="width100 same-padding normal-min-height padding-top overflow">

        <?php
        if(isset($_POST['item_uid']))
        {
        $conn = connDB();
        $itemDetails = getLivestream($conn,"WHERE uid = ? ", array("uid") ,array($_POST['item_uid']),"s");
        ?>

            <form action="utilities/adminLivestreamEditFunction.php" method="POST">
		<div class="dual-input">
        	<p class="top-p">Username</p>
            <input type="text" class="line-input clean" placeholder="Username" value="<?php echo $itemDetails[0]->getUsername();?>" id="username" name="username" required>
       </div>
      <div class="dual-input second-dual-input"> 
      	<p class="top-p">Channel</p>
                    <input type="text" class="line-input clean" placeholder="Channel" value="<?php echo $itemDetails[0]->getChannel();?>" id="channel" name="channel" required>
      </div>
      <div class="clear"></div>             
		<div class="dual-input">
        	<p class="top-p">Date</p>
                <input type="date" class="line-input clean" placeholder="Date" value="<?php echo $itemDetails[0]->getDate();?>" id="date" name="date" required>
	</div>
      <div class="dual-input second-dual-input"> 
      	<p class="top-p">Time Start</p>
                <input type="time" class="line-input clean" placeholder="Time Start" value="<?php echo $itemDetails[0]->getTimeStart();?>" id="time_start" name="time_start" required>
       </div>
      <div class="clear"></div>             
		<div class="dual-input">
        	<p class="top-p">Time End</p>
                <input type="time" class="line-input clean" placeholder="Time End" value="<?php echo $itemDetails[0]->getTimeEnd();?>" id="time_end" name="time_end" required>
</div>
                <div class="clear"></div>   

                <input type="hidden" value="<?php echo $itemDetails[0]->getUid();?>" id="item_uid" name="item_uid" readonly>
				<div class="text-center middle-div-width">
                	<button class="clean yellow-btn edit-profile-width" name="submit">Save</button>   
                </div>
            </form> 

        <?php
        }
        ?>

        </div>
    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>