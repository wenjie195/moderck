<?php
class Withdrawal {
    /* Member variables */
    var $id,$uid,$withdrawalUid,$currentAmount,$amount,$finalAmount,$method,$note,$bankName,$bankAccHolder,$bankAccNo,$contact,$receipt,$status,$reason,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalUid()
    {
        return $this->withdrawalUid;
    }

    /**
     * @param mixed $withdrawalUid
     */
    public function setWithdrawalUid($withdrawalUid)
    {
        $this->withdrawalUid = $withdrawalUid;
    }

    /**
     * @return mixed
     */
    public function getCurrentAmount()
    {
        return $this->currentAmount;
    }

    /**
     * @param mixed $currentAmount
     */
    public function setCurrentAmount($currentAmount)
    {
        $this->currentAmount = $currentAmount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getFinalAmount()
    {
        return $this->finalAmount;
    }

    /**
     * @param mixed $finalAmount
     */
    public function setFinalAmount($finalAmount)
    {
        $this->finalAmount = $finalAmount;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $ibankNamed
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankAccHolder()
    {
        return $this->bankAccHolder;
    }

    /**
     * @param mixed $bankAccHolder
     */
    public function setBankAccHolder($bankAccHolder)
    {
        $this->bankAccHolder = $bankAccHolder;
    }

    /**
     * @return mixed
     */
    public function getBankAccNo()
    {
        return $this->bankAccNo;
    }

    /**
     * @param mixed $bankAccNo
     */
    public function setBankAccNo($bankAccNo)
    {
        $this->bankAccNo = $bankAccNo;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param mixed $receipt
     */
    public function setReceipt($receipt)
    {
        $this->receipt = $receipt;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getWithdrawal($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","withdrawal_uid","current_amount","amount","final_amount","method","note","bank_name","bank_account_holder","bank_account_no",
                                "contact","receipt","status","reason","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"withdrawal");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$withdrawalUid,$currentAmount,$amount,$finalAmount,$method,$note,$bankName,$bankAccHolder,$bankAccNo,$contact,$receipt,$status,$reason,
                                $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Withdrawal();
            $class->setId($id);
            $class->setUid($uid);
            $class->setWithdrawalUid($withdrawalUid);
            $class->setCurrentAmount($currentAmount);
            $class->setAmount($amount);
            $class->setFinalAmount($finalAmount);
            $class->setMethod($method);
            $class->setNote($note);
            $class->setBankName($bankName);
            $class->setBankAccHolder($bankAccHolder);
            $class->setBankAccNo($bankAccNo);
            $class->setContact($contact);
            $class->setReceipt($receipt);
            $class->setStatus($status);
            $class->setReason($reason);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}