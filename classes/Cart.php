<?php  
class Product {
    /* Member variables */
    var $id,$uid,$name,$code,$price,$productValue,$redemptionPoint,$status,$description,$descriptionTwo,$keywordOne,$imageOne,$imageTwo,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getProductValue()
    {
        return $this->productValue;
    }

    /**
     * @param mixed $productValue
     */
    public function setProductValue($productValue)
    {
        $this->productValue = $productValue;
    }

    /**
     * @return mixed
     */
    public function getRedemptionPoint()
    {
        return $this->redemptionPoint;
    }

    /**
     * @param mixed $redemptionPoint
     */
    public function setRedemptionPoint($redemptionPoint)
    {
        $this->redemptionPoint = $redemptionPoint;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescriptionTwo()
    {
        return $this->descriptionTwo;
    }

    /**
     * @param mixed $descriptionTwo
     */
    public function setDescriptionTwo($descriptionTwo)
    {
        $this->descriptionTwo = $descriptionTwo;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }

    /**
     * @param mixed $imageTwo
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","name","code","price","product_value","redemption_point","status","description","description_two","keyword_one",
                            "image_one","image_two","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$name,$code,$price,$productValue,$redemptionPoint,$status,$description,$descriptionTwo,$keywordOne,
                            $imageOne,$imageTwo,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product;
            $class->setId($id);
            $class->setUid($uid);
            $class->setName($name);
            $class->setCode($code);
            $class->setPrice($price);
            $class->setProductValue($productValue);
            $class->setRedemptionPoint($redemptionPoint);
            $class->setStatus($status);
            $class->setDescription($description);
            $class->setDescriptionTwo($descriptionTwo);
            $class->setKeywordOne($keywordOne);
            $class->setImageOne($imageOne);
            $class->setImageTwo($imageTwo);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}


function createProductList($products,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true)
{
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";
    if(!$products)
    {
        return $productListHtml;
    }

    $subtotal = 0;
    $index = 0;
    foreach ($products as $product)
    {
        $quantity = 0;
        if($postQuantityRows)
        {
            $quantity = $postQuantityRows[$index];
        }

        $totalPrice = 0;

        if($quantity <= 0 && !$isIncludeNotSelectedProductToo)
        {
            $productListHtml .= '<div style="display: none;" >';
        }
        else
        {
            $totalPrice = $quantity * $product->getPrice();
            $subtotal += $totalPrice;
            $productListHtml .= '<div style="display:flex;" class="per-product-div">';
        }

        $conn=connDB();
        //$productArray = getProduct($conn);
        $id  = $product->getName();
        // Include the database configuration file

        // Get images from the database
        $query = $conn->query("SELECT image_one FROM product WHERE name = '$id'");

        if($query->num_rows > 0){
            while($row = $query->fetch_assoc()){
                $imageURL = './productImage/'.$row["image_one"];

                    $productListHtml .= '
                    <!-- Product -->
                   

                                <div class="left-cart-img-div left-cart-img-div2">
									<div class="square">
										<div class="content"><a href="'.$imageURL.'" data-fancybox="images-preview">
                                    		<img src="'.$imageURL.'" class="width100" alt="'.$product->getName().'" title="'.$product->getName().'"></a>
										</div>
									</div>
                                </div>

                            
                            <div class="left-product-details">
                                <p class="product-table-p">'.$product->getName().'</p>
							</div>
							<div class="middle-product-details">
                                <p class="product-table-p">'.$quantity.'</p>
							</div>
							<div class="last-product-details">
                                <p class="product-table-p">'.$totalPrice.'</p>
							</div>							
                           
                        </div> 
                
                ';
            }
        }
        $index++;
    }

    $productListHtml .= '
    
    <div class="per-product-div ">
							<div class="price2-div"><p class="product-table-p total-p">Total</p></div>
 							<div class="last-product-details">
                                <p class="product-table-p">'.$subtotal.'</p>
							</div>	           
           
           
	</div>
            <div class="clear"></div>  

            <div class="login-div margin-auto">                                                                     
                <button class="clean white-button ow-red-bg white-text confirm-btn">Checkout</button>
            </div>

            <div class="width100 overflow text-center margin-bottom-5px margin-top-5px">
                <a href="eCommerceSite.php" class="clean white-to-tur" >Continue Shopping</a>
            </div>
    </div>

';

    return $productListHtml;
}

function getProductPrice($conn,$productId)
{
    $price = 0;
    $productPriceRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");
    if($productPriceRows)
    {
        $price = $productPriceRows[0]->getPrice();
    }
    return $price;
}

function getProductPV($conn,$productId)
{
    $productPV = 0;
    $productPVRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");
    if($productPVRows)
    {
        $productPV = $productPVRows[0]->getProductValue();
    }
    return $productPV;
}

function getProductRP($conn,$productId)
{
    $productRP = 0;
    $productRPRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");
    if($productRPRows)
    {
        $productRP = $productRPRows[0]->getRedemptionPoint();
    }
    return $productRP;
}

function getProductName($conn,$productId)
{
    $name = "";
    $productNameRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");
    if($productNameRows)
    {
        $name = $productNameRows[0]->getName();
    }
    return $name;
}

function getProductUid($conn,$productId)
{
    $productUid = "";
    $productUidRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");
    if($productUidRows)
    {
        $productUid = $productUidRows[0]->getUid();
    }
    return $productUid;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

    //    if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
    //    }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];
        $order_Id = md5(uniqid());

        $orderId = insertDynamicData($conn,"orders",array("uid","order_id"),array($uid,$order_Id),"ss");

        // $orderUID = md5(uniqid());

        // $orderId = insertDynamicData($conn,"orders",array("uid"),array($orderUID),"s");
        //$orderId = insertDynamicData($conn,"orders",array("uid","username","full_name"),array($uid,$username,$fullName),"sss");

        if($orderId){
            $totalPrice = 0;
            $totalProductValue = 0;
            $totalRedemptionPoint = 0;

            for($index = 0; $index < count($shoppingCart); $index++){
                
                $totalPrice =0;
                $totalProductValue = 0;
                $totalRedemptionPoint = 0;

                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                $quantity = $thisCart['quantity'];
                $originalPrice = getProductPrice($conn,$productId);
                $totalPrice += ($originalPrice * $quantity);

                $productName = getProductName($conn,$productId);
                $productUid = getProductUid($conn,$productId);
                $mainProductUid = $productUid;

                $productValue = getProductPV($conn,$productId);
                $totalProductValue += ($productValue * $quantity);

                $redemptionPoint = getProductRP($conn,$productId);
                $totalRedemptionPoint += ($redemptionPoint * $quantity);

                $status = "Pending";

                if($quantity <= 0){
                    continue;
                }

                // if(!insertDynamicData($conn,"product_orders",array("product_id","product_name","order_id","quantity","final_price","original_price","discount_given","totalProductPrice","totalCommission"),
                //     array($productId,$productName,$orderId,$quantity,$originalPrice,$originalPrice,0,$totalPrice,$totalCommission),"isiidddds")){
                //     promptError("error creating order for product : $productId");
                // }

                if(!insertDynamicData($conn,"order_list",array("order_id","user_uid","product_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid","product_value","redemption_point"),
                array($order_Id,$uid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalProductValue,$totalRedemptionPoint),"sssssssssss"))
                {
                    promptError("error creating order for product : $productId");
                }

            }

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getProduct($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}
