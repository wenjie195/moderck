<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $preOrderId = rewrite($_POST["preorder_id"]);
    $preOrderQuantity = rewrite($_POST["product_quantity"]);
    $productPrice = rewrite($_POST["product_price"]);

    $productValue = rewrite($_POST["product_value"]);
    $redemptionPoint = rewrite($_POST["redemption_point"]);

    $addOn = 1;
    $updatePreOrderQuantity = $preOrderQuantity - $addOn ;
    $updateProductSubtotalPrice = ($productPrice *  $updatePreOrderQuantity);

    $updateProductValue = ($productValue *  $updatePreOrderQuantity);
    $updateRedemptionPoint = ($redemptionPoint *  $updatePreOrderQuantity);

    $updateStatus = "Delete";

    $updateProductPriceZero = 0;
    $updateProductQuantityZero = 0;

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    // if($updatePreOrderQuantity < 1)
    // {}
    // else
    // {}

    $preOrderDetails = getPreOrderList($conn," WHERE id = ? ",array("id"),array($preOrderId),"s");    

    if($updatePreOrderQuantity < 1)
    {
        if($preOrderDetails)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
    
            if($updateStatus)
            {
                array_push($tableName,"status");
                array_push($tableValue,$updateStatus);
                $stringType .=  "s";
            }

            if(!$updateProductPriceZero || $updateProductPriceZero)
            {
                array_push($tableName,"totalPrice");
                array_push($tableValue,$updateProductPriceZero);
                $stringType .=  "s";
            }

            if(!$updateProductQuantityZero || $updateProductQuantityZero)
            {
                array_push($tableName,"quantity");
                array_push($tableValue,$updateProductQuantityZero);
                $stringType .=  "s";
            }

            // if($updateProductValue)
            if(!$updateProductValue || $updateProductValue)
            {
                array_push($tableName,"product_value");
                array_push($tableValue,$updateProductValue);
                $stringType .=  "s";
            }
            // if($updateRedemptionPoint)
            if(!$updateRedemptionPoint || $updateRedemptionPoint)
            {
                array_push($tableName,"redemption_point");
                array_push($tableValue,$updateRedemptionPoint);
                $stringType .=  "s";
            }
        
            array_push($tableValue,$preOrderId);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"preorder_list"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // $_SESSION['messageType'] = 1;
                header('Location: ../shoppingCart.php');
            }
            else
            {
                // $_SESSION['messageType'] = 1;
                // header('Location: ../viewShoppingCart.php?type=2');
                echo "FAIL";
            }
        }
        else
        {
            echo "ERROR";
        }
    }
    else
    {
        if($preOrderDetails)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
    
            if($updatePreOrderQuantity)
            {
                array_push($tableName,"quantity");
                array_push($tableValue,$updatePreOrderQuantity);
                $stringType .=  "s";
            }
    
            if($updateProductSubtotalPrice)
            {
                array_push($tableName,"totalPrice");
                array_push($tableValue,$updateProductSubtotalPrice);
                $stringType .=  "s";
            }

            if(!$updateProductValue || $updateProductValue)
            {
                array_push($tableName,"product_value");
                array_push($tableValue,$updateProductValue);
                $stringType .=  "s";
            }
            if(!$updateRedemptionPoint || $updateRedemptionPoint)
            {
                array_push($tableName,"redemption_point");
                array_push($tableValue,$updateRedemptionPoint);
                $stringType .=  "s";
            }
    
            array_push($tableValue,$preOrderId);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"preorder_list"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // $_SESSION['messageType'] = 1;
                header('Location: ../shoppingCart.php');
            }
            else
            {
                // $_SESSION['messageType'] = 1;
                // header('Location: ../viewShoppingCart.php?type=2');
                echo "FAIL";
            }
        }
        else
        {
            echo "ERROR";
        }
    }
}
else 
{
    header('Location: ../index.php');
}
?>