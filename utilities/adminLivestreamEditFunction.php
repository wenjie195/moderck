<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Livestream.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);

     $username = rewrite($_POST['username']);
     $channel = rewrite($_POST['channel']);
     $date = rewrite($_POST['date']);
     $timeStart = rewrite($_POST['time_start']);
     $timeEnd = rewrite($_POST['time_end']);

     $liveStreamDetails = getLivestream($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($liveStreamDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($username)
          {
               array_push($tableName,"username");
               array_push($tableValue,$username);
               $stringType .=  "s";
          }
          if($channel)
          {
               array_push($tableName,"channel");
               array_push($tableValue,$channel);
               $stringType .=  "s";
          }
          if($date)
          {
               array_push($tableName,"date");
               array_push($tableValue,$date);
               $stringType .=  "s";
          }
          if($timeStart)
          {
               array_push($tableName,"time_start");
               array_push($tableValue,$timeStart);
               $stringType .=  "s";
          }
          if($timeEnd)
          {
               array_push($tableName,"time_end");
               array_push($tableValue,$timeEnd);
               $stringType .=  "s";
          }

          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $announcementUpdated = updateDynamicData($conn,"livestream"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($announcementUpdated)
          {
               // echo "Success";
               header('Location: ../adminLivestreamAll.php');
          }
          else
          {
               echo "Fail";
          }
     }
     else
     {
          echo "Error";
     }

}
else
{
     header('Location: ../index.php');
}
?>