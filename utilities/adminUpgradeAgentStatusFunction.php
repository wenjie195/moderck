<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Upgrading.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$editBy = $_SESSION['uid'];

function addUpgrading($conn,$progressUid,$userUid,$editBy,$ip_address)
{
    if(insertDynamicData($conn,"upgrading",array("uid","user_uid","edit_by","ip_address"),
    array($progressUid,$userUid,$editBy,$ip_address),"ssss") === null)
    {
        echo "gg";
    }
    else{    }
    return true;
}



//whether ip is from share internet
if (!empty($_SERVER['HTTP_CLIENT_IP']))   
{
    $ip_address = $_SERVER['HTTP_CLIENT_IP'];
}
//whether ip is from proxy
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
{
    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
//whether ip is from remote address
else
{
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
$ip_address;



if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $progressUid = md5(uniqid());

     $itemUid = rewrite($_POST['item_uid']);
     $userUid = $itemUid;

     $rank = "Agent";
     $achieveRank = "Agent";

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($userDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($rank)
          {
               array_push($tableName,"rank");
               array_push($tableValue,$rank);
               $stringType .=  "s";
          }
          if($achieveRank)
          {
               array_push($tableName,"rank");
               array_push($tableValue,$achieveRank);
               $stringType .=  "s";
          }

          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $announcementUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($announcementUpdated)
          {
               // header('Location: ../adminMemberOverview.php');
               if(addUpgrading($conn,$progressUid,$userUid,$editBy,$ip_address))
               {
                    header('Location: ../adminMemberOverview.php');
               }
          }
          else
          {
               echo "Fail";
          }
     }
     else
     {
          echo "Error";
     }

}
else
{
     header('Location: ../index.php');
}
?>