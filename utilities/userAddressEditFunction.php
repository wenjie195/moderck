<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Address.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $userUid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);
     $userUid = rewrite($_POST['user_uid']);

     $recipient = rewrite($_POST['recipient_name']);
     $mobile = rewrite($_POST['mobile_no']);
     $houseRoad = rewrite($_POST['address']);
     $city = rewrite($_POST['city']);
     $state = rewrite($_POST['state']);
     $postcode = rewrite($_POST['postcode']);
     $country = rewrite($_POST['country']);

     $defaultShip = $_POST['default_ship'];
     $defaultBill = $_POST['default_bill'];

     if($defaultShip == "Yes")
     {
          if($defaultBill == "Yes")
          {

               $allUserAddress = getAddress($conn, "WHERE user_uid = ? ", array("user_uid"), array($userUid), "s");
               if($allUserAddress)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
     
                    $updateDefaultShipStatus = "No";
                    $updateDefaultBillStatus = "No";
     
                    if($updateDefaultShipStatus)
                    {
                         array_push($tableName,"default_ship");
                         array_push($tableValue,$updateDefaultShipStatus);
                         $stringType .=  "s";
                    }  
                    if($updateDefaultBillStatus)
                    {
                         array_push($tableName,"default_bill");
                         array_push($tableValue,$updateDefaultBillStatus);
                         $stringType .=  "s";
                    }  
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    // $updateAddressBothYes = updateDynamicData($conn,"address"," WHERE user_uid = ? AND status = 'Available' ",$tableName,$tableValue,$stringType);
                    $updateAddressBothYes = updateDynamicData($conn,"address"," WHERE user_uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateAddressBothYes)
                    {

                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
               
                         if($recipient)
                         {
                              array_push($tableName,"recipient");
                              array_push($tableValue,$recipient);
                              $stringType .=  "s";
                         }
                         if($mobile)
                         {
                              array_push($tableName,"mobile");
                              array_push($tableValue,$mobile);
                              $stringType .=  "s";
                         }
                         if($houseRoad)
                         {
                              array_push($tableName,"house_road");
                              array_push($tableValue,$houseRoad);
                              $stringType .=  "s";
                         }
                         if($city)
                         {
                              array_push($tableName,"city");
                              array_push($tableValue,$city);
                              $stringType .=  "s";
                         }
                         if($state)
                         {
                              array_push($tableName,"state");
                              array_push($tableValue,$state);
                              $stringType .=  "s";
                         }
                         if($postcode)
                         {
                              array_push($tableName,"postcode");
                              array_push($tableValue,$postcode);
                              $stringType .=  "s";
                         }
                         if($country)
                         {
                              array_push($tableName,"country");
                              array_push($tableValue,$country);
                              $stringType .=  "s";
                         }
                         if($defaultShip)
                         {
                              array_push($tableName,"default_ship");
                              array_push($tableValue,$defaultShip);
                              $stringType .=  "s";
                         }
                         if($defaultBill)
                         {
                              array_push($tableName,"default_bill");
                              array_push($tableValue,$defaultBill);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$itemUid);
                         $stringType .=  "s";
                         $addressUpdated = updateDynamicData($conn,"address"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($addressUpdated)
                         {
                              header('Location: ../userAddressBook.php');
                         }
                         else
                         {
                              echo "Fail";
                         }

                    }
                    else
                    {
                         echo "Error 1(A1)";
                         echo "<br>";
                    }
               }

          }
          elseif($defaultBill == "No")
          {

               $allUserAddress = getAddress($conn, "WHERE user_uid = ? ", array("user_uid"), array($userUid), "s");
               if($allUserAddress)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
     
                    $updateDefaultShipStatus = "No";
                    // $updateDefaultBillStatus = "No";
     
                    if($updateDefaultShipStatus)
                    {
                         array_push($tableName,"default_ship");
                         array_push($tableValue,$updateDefaultShipStatus);
                         $stringType .=  "s";
                    }  
                    // if($updateDefaultBillStatus)
                    // {
                    //      array_push($tableName,"default_bill");
                    //      array_push($tableValue,$updateDefaultBillStatus);
                    //      $stringType .=  "s";
                    // }  
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    // $updateAddressBothYes = updateDynamicData($conn,"address"," WHERE user_uid = ? AND status = 'Available' ",$tableName,$tableValue,$stringType);
                    $updateAddressYesNo = updateDynamicData($conn,"address"," WHERE user_uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateAddressYesNo)
                    {

                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
               
                         if($recipient)
                         {
                              array_push($tableName,"recipient");
                              array_push($tableValue,$recipient);
                              $stringType .=  "s";
                         }
                         if($mobile)
                         {
                              array_push($tableName,"mobile");
                              array_push($tableValue,$mobile);
                              $stringType .=  "s";
                         }
                         if($houseRoad)
                         {
                              array_push($tableName,"house_road");
                              array_push($tableValue,$houseRoad);
                              $stringType .=  "s";
                         }
                         if($city)
                         {
                              array_push($tableName,"city");
                              array_push($tableValue,$city);
                              $stringType .=  "s";
                         }
                         if($state)
                         {
                              array_push($tableName,"state");
                              array_push($tableValue,$state);
                              $stringType .=  "s";
                         }
                         if($postcode)
                         {
                              array_push($tableName,"postcode");
                              array_push($tableValue,$postcode);
                              $stringType .=  "s";
                         }
                         if($country)
                         {
                              array_push($tableName,"country");
                              array_push($tableValue,$country);
                              $stringType .=  "s";
                         }
                         if($defaultShip)
                         {
                              array_push($tableName,"default_ship");
                              array_push($tableValue,$defaultShip);
                              $stringType .=  "s";
                         }
                         if($defaultBill)
                         {
                              array_push($tableName,"default_bill");
                              array_push($tableValue,$defaultBill);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$itemUid);
                         $stringType .=  "s";
                         $addressUpdated = updateDynamicData($conn,"address"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($addressUpdated)
                         {
                              header('Location: ../userAddressBook.php');
                         }
                         else
                         {
                              echo "Fail";
                         }

                    }
                    else
                    {
                         echo "Error 1(A1)";
                         echo "<br>";
                    }
               }

          }
     }
     elseif($defaultShip == "No")
     {
          if($defaultBill == "Yes")
          {

               $allUserAddress = getAddress($conn, "WHERE user_uid = ? ", array("user_uid"), array($userUid), "s");
               if($allUserAddress)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
     
                    // $updateDefaultShipStatus = "No";
                    $updateDefaultBillStatus = "No";
     
                    // if($updateDefaultShipStatus)
                    // {
                    //      array_push($tableName,"default_ship");
                    //      array_push($tableValue,$updateDefaultShipStatus);
                    //      $stringType .=  "s";
                    // }  
                    if($updateDefaultBillStatus)
                    {
                         array_push($tableName,"default_bill");
                         array_push($tableValue,$updateDefaultBillStatus);
                         $stringType .=  "s";
                    }  
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    // $updateAddressBothYes = updateDynamicData($conn,"address"," WHERE user_uid = ? AND status = 'Available' ",$tableName,$tableValue,$stringType);
                    $updateAddressNoYes = updateDynamicData($conn,"address"," WHERE user_uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateAddressNoYes)
                    {

                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
               
                         if($recipient)
                         {
                              array_push($tableName,"recipient");
                              array_push($tableValue,$recipient);
                              $stringType .=  "s";
                         }
                         if($mobile)
                         {
                              array_push($tableName,"mobile");
                              array_push($tableValue,$mobile);
                              $stringType .=  "s";
                         }
                         if($houseRoad)
                         {
                              array_push($tableName,"house_road");
                              array_push($tableValue,$houseRoad);
                              $stringType .=  "s";
                         }
                         if($city)
                         {
                              array_push($tableName,"city");
                              array_push($tableValue,$city);
                              $stringType .=  "s";
                         }
                         if($state)
                         {
                              array_push($tableName,"state");
                              array_push($tableValue,$state);
                              $stringType .=  "s";
                         }
                         if($postcode)
                         {
                              array_push($tableName,"postcode");
                              array_push($tableValue,$postcode);
                              $stringType .=  "s";
                         }
                         if($country)
                         {
                              array_push($tableName,"country");
                              array_push($tableValue,$country);
                              $stringType .=  "s";
                         }
                         if($defaultShip)
                         {
                              array_push($tableName,"default_ship");
                              array_push($tableValue,$defaultShip);
                              $stringType .=  "s";
                         }
                         if($defaultBill)
                         {
                              array_push($tableName,"default_bill");
                              array_push($tableValue,$defaultBill);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$itemUid);
                         $stringType .=  "s";
                         $addressUpdated = updateDynamicData($conn,"address"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($addressUpdated)
                         {
                              header('Location: ../userAddressBook.php');
                         }
                         else
                         {
                              echo "Fail";
                         }

                    }
                    else
                    {
                         echo "Error 1(A1)";
                         echo "<br>";
                    }
               }

          }
          elseif($defaultBill == "No")
          {

               $allUserAddress = getAddress($conn, "WHERE user_uid = ? ", array("user_uid"), array($userUid), "s");
               if($allUserAddress)
               {

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
          
                    if($recipient)
                    {
                         array_push($tableName,"recipient");
                         array_push($tableValue,$recipient);
                         $stringType .=  "s";
                    }
                    if($mobile)
                    {
                         array_push($tableName,"mobile");
                         array_push($tableValue,$mobile);
                         $stringType .=  "s";
                    }
                    if($houseRoad)
                    {
                         array_push($tableName,"house_road");
                         array_push($tableValue,$houseRoad);
                         $stringType .=  "s";
                    }
                    if($city)
                    {
                         array_push($tableName,"city");
                         array_push($tableValue,$city);
                         $stringType .=  "s";
                    }
                    if($state)
                    {
                         array_push($tableName,"state");
                         array_push($tableValue,$state);
                         $stringType .=  "s";
                    }
                    if($postcode)
                    {
                         array_push($tableName,"postcode");
                         array_push($tableValue,$postcode);
                         $stringType .=  "s";
                    }
                    if($country)
                    {
                         array_push($tableName,"country");
                         array_push($tableValue,$country);
                         $stringType .=  "s";
                    }
                    if($defaultShip)
                    {
                         array_push($tableName,"default_ship");
                         array_push($tableValue,$defaultShip);
                         $stringType .=  "s";
                    }
                    if($defaultBill)
                    {
                         array_push($tableName,"default_bill");
                         array_push($tableValue,$defaultBill);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$itemUid);
                    $stringType .=  "s";
                    $addressUpdated = updateDynamicData($conn,"address"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($addressUpdated)
                    {
                         header('Location: ../userAddressBook.php');
                    }
                    else
                    {
                         echo "Fail";
                    }

               }

          }
     }
     else
     {
          echo "Error (Yes / No)";
     }
  
}
else 
{
     header('Location: ../index.php');
}
?>