<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/Level.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


function directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlSeven($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl7,$upline7thUsername,$bonusLvlSeven,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl7,$upline7thUsername,$bonusLvlSeven,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}





if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     echo $orderUid = rewrite($_POST["order_uid"]);
     echo "<br>";  

     $purchaserOrderDetails = getOrders($conn, " WHERE order_id = '$orderUid' ");
     echo $purchaserUid = $purchaserOrderDetails[0]->getUid();
     echo "<br>";
     echo "<br>";
     // echo $orderUserName = $orderDetails[0]->getName();
     // echo "<br>";

     $orderListDetails = getOrderList($conn, " WHERE order_id =? ",array("order_id"),array($orderUid),"s");
     if($orderListDetails)
     {
          $totalProductValue = 0;
          $totalRedemptionPoint = 0;
          for($cnt = 0;$cnt < count($orderListDetails) ;$cnt++)
          {
               echo $orderListDetails[$cnt]->getProductName();
               echo "<br>";
               $totalProductValue += $orderListDetails[$cnt]->getProductValue();
               $totalRedemptionPoint += $orderListDetails[$cnt]->getRedemptionPoint();
          }

          // $totalAmount = 0; // initital
          // for ($cnt=0; $cnt <count($orderListDetails) ; $cnt++)
          // {
          //     $totalAmount += $orderListDetails[$b]->getProductValue();
          // }
     }

     // echo $totalProductValue;
     // echo $totalRedemptionPoint;

     echo "Total Product Value :";
     echo $totalProductValue;
     echo "<br>";
     echo "Total Redemption Point :";
     echo $totalRedemptionPoint;
     echo "<br>";
     echo "<br>";

     $purchaserDetails = getUser($conn, " WHERE uid = ? ",array("uid"),array($purchaserUid),"s");
     echo $purchaserDetails[0]->getUsername();
     echo "   (";
     echo $purchaserDetails[0]->getLastname();
     echo $purchaserDetails[0]->getFirstname();
     echo ")<br>";
     echo $firstOrder = $purchaserDetails[0]->getFirstOrder();
     echo "<br>";
     echo $purchaserDetails[0]->getRank();
     echo "<br>";

     // user details in referral history
     $userRH = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($purchaserUid),"s");
     echo $userLevel = $userRH[0]->getCurrentLevel();
     echo "<br>";
     echo "<br>";

     //Direct Upline / Level 1
     echo $directUplineUid = $userRH[0]->getReferrerId();
     echo "<br>";  
     $directUplineDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($directUplineUid),"s");
     $directUplineUid = $directUplineDetails[0]->getUid();
     // echo "<br>";  
     echo $directUplineUsername = $directUplineDetails[0]->getUsername();
     echo "<br>";  
     echo $directUplineRank = $directUplineDetails[0]->getRank();
     echo "<br>";  
     echo $directUplineSalesCommission = $directUplineDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $directUplineRedemptionPoint = $directUplineDetails[0]->getRedemptionPoint();
     echo "<br>";  
     echo "<br>";  


     //Level 2
     $uplineTwoRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($directUplineUid),"s");
     echo $uplineTwoUid = $uplineTwoRows[0]->getReferrerId();
     echo "<br>";  
     $uplineTwoDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineTwoUid),"s");
     echo $uplineTwoUsername = $uplineTwoDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineTwoRank = $uplineTwoDetails[0]->getRank();
     echo "<br>";  
     echo $uplineTwoSalesCommission = $uplineTwoDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineTwoRedemptionPoint = $uplineTwoDetails[0]->getRedemptionPoint();
     echo "<br>";  
     echo "<br>";

     //Level 3
     $uplineThreeRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineTwoUid),"s");
     echo $uplineThreeUid = $uplineThreeRows[0]->getReferrerId();
     echo "<br>";  
     $uplineThreeDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineThreeUid),"s");
     echo $uplineThreeUsername = $uplineThreeDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineThreeRank = $uplineThreeDetails[0]->getRank();
     echo "<br>";  
     echo $uplineThreeSalesCommission = $uplineThreeDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineThreeRedemptionPoint = $uplineThreeDetails[0]->getRedemptionPoint();
     echo "<br>";  
     echo "<br>";

     //Level 4
     $uplineFourRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineThreeUid),"s");
     echo $uplineFourUid = $uplineFourRows[0]->getReferrerId();
     echo "<br>";  
     $uplineFourDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineFourUid),"s");
     echo $uplineFourUsername = $uplineFourDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineFourRank = $uplineFourDetails[0]->getRank();
     echo "<br>";  
     echo $uplineFourSalesCommission = $uplineFourDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineFourRedemptionPoint = $uplineFourDetails[0]->getRedemptionPoint();
     echo "<br>";  
     echo "<br>";

     //Level 5
     $uplineFiveRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineFourUid),"s");
     echo $uplineFiveUid = $uplineFiveRows[0]->getReferrerId();
     echo "<br>";  
     $uplineFiveDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineFiveUid),"s");
     echo $uplineFiveUsername = $uplineFiveDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineFiveRank = $uplineFiveDetails[0]->getRank();
     echo "<br>";  
     echo $uplineFiveSalesCommission = $uplineFiveDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineFiveRedemptionPoint = $uplineFiveDetails[0]->getRedemptionPoint();
     echo "<br>";  
     echo "<br>";

     //Level 6
     $uplineSixRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineFiveUid),"s");
     echo $uplineSixUid = $uplineSixRows[0]->getReferrerId();
     echo "<br>";  
     $uplineSixDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineSixUid),"s");
     echo $uplineSixUsername = $uplineSixDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineSixRank = $uplineSixDetails[0]->getRank();
     echo "<br>";  
     echo $uplineSixSalesCommission = $uplineSixDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineSixRedemptionPoint = $uplineSixDetails[0]->getRedemptionPoint();
     echo "<br>";  
     echo "<br>";

     //Level 7
     $uplineSevenRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineSixUid),"s");
     echo $uplineSevenUid = $uplineSevenRows[0]->getReferrerId();
     echo "<br>";  
     $uplineSevenDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineSevenUid),"s");
     echo $uplineSevenUsername = $uplineSevenDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineSevenRank = $uplineSevenDetails[0]->getRank();
     echo "<br>";  
     echo $uplineSevenSalesCommission = $uplineSevenDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineSevenRedemptionPoint = $uplineSevenDetails[0]->getRedemptionPoint();
     echo "<br>";  
     echo "<br>";

     $paymentStatus = 'APPROVED';
     $monthlyBonusStatus = 'PENDING';

     if($firstOrder == 0)
     {

     }
     else
     {

     }

}
else 
{
    header('Location: ../index.php');
}
?>