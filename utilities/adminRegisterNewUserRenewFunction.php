<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt"),
          array($uid,$username,$email,$finalPassword,$salt),"sssss") === null)
     {
          echo "GG";
     }
     else
     {    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function addProductOne($conn,$uid,$productOneUid,$productOneName,$status)
{
     if(insertDynamicData($conn,"preorder_list",array("user_uid","product_uid","product_name","status"),
          array($uid,$productOneUid,$productOneName,$status),"ssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function addProductTwo($conn,$uid,$productTwoUid,$productTwoName,$status)
{
     if(insertDynamicData($conn,"preorder_list",array("user_uid","product_uid","product_name","status"),
          array($uid,$productTwoUid,$productTwoName,$status),"ssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function addProductThree($conn,$uid,$productThreeUid,$productThreeName,$status)
{
     if(insertDynamicData($conn,"preorder_list",array("user_uid","product_uid","product_name","status"),
          array($uid,$productThreeUid,$productThreeName,$status),"ssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

// function sendEmailForVerification($uid)
// {
//      $conn = connDB();
//      $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

//      $verifyUser_debugMode = 2;
//      $verifyUser_host = "mail.hygeniegroup.com";
//      $verifyUser_usernameThatSendEmail = "noreply@hygeniegroup.com";                   // Sender Acc Username
//      $verifyUser_password = "M7vXtsRMSJO8";                                              // Sender Acc Password

//      $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
//      $verifyUser_port = 465;                                                              // SMTP port no
//      $verifyUser_sentFromThisEmailName = "noreply@hygeniegroup.com";                    // Sender Username
//      $verifyUser_sentFromThisEmail = "noreply@hygeniegroup.com";                        // Sender Email


//      $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                            // Recipient Username
//      $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
//      $verifyUser_isHtml = true;                                                                // Set To Html
//      $verifyUser_subject = "Welcome To Hygenie Group";

//      $verifyUser_body = "<p>Dear ".$userRows[0]->getUsername().",</p>";                      // Body
//      $verifyUser_body .="<p>You have successfully registered for Hygenie Group.</p>";
//      $verifyUser_body .="<p>Here are your temporary login details :</p>";
//      $verifyUser_body .="<p>https://hygeniegroup.com/</p>";
//      $verifyUser_body .="<p>Username : ".$userRows[0]->getUsername()."</p>";
//      $verifyUser_body .="<p>Password : 111111</p>";
//      $verifyUser_body .="<p>Thank you.</p>";

//     sendMailTo(
//          null,
//          $verifyUser_host,
//          $verifyUser_usernameThatSendEmail,
//          $verifyUser_password,
//          $verifyUser_smtpSecure,
//          $verifyUser_port,
//          $verifyUser_sentFromThisEmailName,
//          $verifyUser_sentFromThisEmail,
//          $verifyUser_sendToThisEmailName,
//          $verifyUser_sendToThisEmail,
//          $verifyUser_isHtml,
//          $verifyUser_subject,
//          $verifyUser_body,
//          null
//     );
// }


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['username']);
     $email = rewrite($_POST['email']);

     $register_password = rewrite($_POST['password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     $sponsorID = rewrite($_POST['upline_uid']);

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";

     // $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($icno),"s");
     $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
     $existingUsername = $usernameRows[0];

     if(!$existingUsername)
     {
          
          if($sponsorID)
          {
               $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorID),"s");
               if($referrerUserRows)
               {
                    $referrerUid = $referrerUserRows[0]->getUid();
                    $referrerName = $referrerUserRows[0]->getUsername();
                    // $referralUid = $referrerUserRows[0]->getUid();
                    $referralName = $username;
                    $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                    $currentLevel = 1;
                    $getUplineCurrentLevel = 1;

                    $currentRaking = $referrerUserRows[0]->getRank();
                    
                    // $currentValueDownlineLvlOne = $referrerUserRows[0]->getDownlineLvlOne();
                    // echo $currentValueDownline;

                    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
                    if($referralHistoryRows)
                    {
                         $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                         $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                    }
                    $referralNewestRows = getReferralHistory($conn,"WHERE referral_id = ?", array("referral_id"),array($referrerUid), "s");
                    if($referralNewestRows)
                    {
                         $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;

                         if($register_password == $register_retype_password)
                         {
                              if($register_password_validation >= 6)
                              {  
                                   if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt))
                                   {
                                        // echo "SUCCESS";
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
                                             // header('Location: ../index.php');
                                             // $_SESSION['messageType'] = 1;
                                             // header('Location: ../adminMemberOverview.php?type=1');

                                             $productOneUid = '35117b1cb67c78efa860c4882c2e5745';
                                             $productOneName = "Verju Foryn Deer Placenta (30s)";
                                             // $productOneName = "Verju Foryn Deer Placenta (30's)";

                                             $productTwoUid = '547297ffdb9f49d6e5ff1db5208f2530';
                                             $productTwoName = "Verju Foryn Deer Placenta (60s)";
                                             // $productTwoName = "Verju Foryn Deer Placenta (60's)";

                                             $productThreeUid = '712e761968f09667885752f04d48c77f';
                                             $productThreeName = "Verju Foryn Deer Placenta (150s)";
                                             // $productThreeName = "Verju Foryn Deer Placenta (150's)";

                                             $status = 'PENDING';

                                             if(addProductOne($conn,$uid,$productOneUid,$productOneName,$status))
                                             {
                                                  if(addProductTwo($conn,$uid,$productTwoUid,$productTwoName,$status))
                                                  {
                                                       if(addProductThree($conn,$uid,$productThreeUid,$productThreeName,$status))
                                                       {
                                                            // $_SESSION['messageType'] = 1;
                                                            // header('Location: ../adminMemberOverview.php?type=1');
                                                            echo "hahaha";
                                                       }
                                                       else
                                                       {}
                                                  }
                                                  else
                                                  {}
                                             }
                                             else
                                             {}

                                        }
                                        else
                                        {
                                             echo "FAIL";
                                        }
                                   }
                                   else
                                   {
                                        echo "FAIL";
                                   }
                              }
                              else
                              {
                                   echo "Password < 6";
                              }
                         }
                         else
                         {
                              echo "Password != Retype Passowrd";
                         }

                    }
                    else
                    {
                         echo "<script>alert('register error with referral !'); window.history.go(-1);</script>";
                    }
               }
               else
               {
                    echo "<script>alert('unable to find related data sponsor ID!!'); window.history.go(-1);</script>";
               }
          }
          else
          {
               echo "<script>alert('sponsor ID is unavailible !!'); window.history.go(-1);</script>";
          }

     }
     else
     {
          echo "<script>alert('usernaem has been used by others'); window.history.go(-1);</script>";
     }

     // if($register_password == $register_retype_password)
     // {
     //      if($register_password_validation >= 6)
     //      {  
     //           if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt))
     //           {
     //                echo "SUCCESS";
     //           }
     //           else
     //           {
     //                echo "FAIL";
     //           }
     //      }
     //      else
     //      {
     //           echo "Password < 6";
     //      }
     // }
     // else
     // {
     //      echo "Password != Retype Passowrd";
     // }

     // if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt))
     // {}
     // else
     // {}

}
else
{
     header('Location: ../index.php');
}
?>