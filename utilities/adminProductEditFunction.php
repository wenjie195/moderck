<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);

     $productName = rewrite($_POST['product_name']);
     $productCode = rewrite($_POST['product_code']);
     $sellingPrice = rewrite($_POST['selling_price']);
     $productValue = rewrite($_POST['product_value']);
     $redemptionPoint = rewrite($_POST['redemption_point']);
     // $description = NULL;
     $description = rewrite($_POST['description']);

     $productDetails = getProduct($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($productDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($productName)
          {
               array_push($tableName,"name");
               array_push($tableValue,$productName);
               $stringType .=  "s";
          }
          if($productCode)
          {
               array_push($tableName,"code");
               array_push($tableValue,$productCode);
               $stringType .=  "s";
          }
          if($sellingPrice)
          {
               array_push($tableName,"price");
               array_push($tableValue,$sellingPrice);
               $stringType .=  "s";
          }
          if($productValue)
          {
               array_push($tableName,"product_value");
               array_push($tableValue,$productValue);
               $stringType .=  "s";
          }
          if($redemptionPoint)
          {
               array_push($tableName,"redemption_point");
               array_push($tableValue,$redemptionPoint);
               $stringType .=  "s";
          }

          if($description)
          {
               array_push($tableName,"description");
               array_push($tableValue,$description);
               $stringType .=  "s";
          }

          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $announcementUpdated = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($announcementUpdated)
          {
               // echo "Product Details Updated !";
               header('Location: ../adminProductAll.php');
          }
          else
          {
               echo "Fail";
          }
     }
     else
     {
          echo "Error";
     }

}
else
{
     header('Location: ../index.php');
}
?>