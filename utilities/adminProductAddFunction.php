<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $userUid = $_SESSION['uid'];
// $userUid = "testing123";

function addProduct($conn,$uid,$productName,$productCode,$sellingPrice,$productValue,$redemptionPoint,$description,$status)
{
     if(insertDynamicData($conn,"product",array("uid","name","code","price","product_value","redemption_point","description","status"),
          array($uid,$productName,$productCode,$sellingPrice,$productValue,$redemptionPoint,$description,$status),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();
     $uid = md5(uniqid());

     $productName = rewrite($_POST['product_name']);
     $productCode = rewrite($_POST['product_code']);
     $sellingPrice = rewrite($_POST['selling_price']);
     $productValue = rewrite($_POST['product_value']);
     $redemptionPoint = rewrite($_POST['redemption_point']);

     // $description = NULL;
     $description = rewrite($_POST['description']);

     $status = "Available";

     if(addProduct($conn,$uid,$productName,$productCode,$sellingPrice,$productValue,$redemptionPoint,$description,$status))
     {
          // echo "Success";
          header('Location: ../adminProductAll.php');
     }
     else
     {
          echo "Error";
     }
}
else
{
     header('Location: ../index.php');
}
?>