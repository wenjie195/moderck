<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = $userID;

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $bankName = rewrite($_POST['bank_name']);
     $accHolderName = rewrite($_POST['acc_holder_name']);
     $accNo = rewrite($_POST['acc_no']);
     $nric = rewrite($_POST['nric']);

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $uid."<br>";
    //  echo $username."<br>";

     if($userDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($bankName)
          {
               array_push($tableName,"bank_name");
               array_push($tableValue,$bankName);
               $stringType .=  "s";
          }
          if($accHolderName)
          {
               array_push($tableName,"bank_acc_name");
               array_push($tableValue,$accHolderName);
               $stringType .=  "s";
          }
          if($accNo)
          {
               array_push($tableName,"bank_acc_number");
               array_push($tableValue,$accNo);
               $stringType .=  "s";
          }
          if($nric)
          {
               array_push($tableName,"icno");
               array_push($tableValue,$nric);
               $stringType .=  "s";
          }
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $articleUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($articleUpdated)
          {
               echo "Success";
          }
          else
          {
               echo "Fail";
               // $_SESSION['messageType'] = 2;
               // header('Location: ../approvedArticle.php?type=2');
          }
     }
     else
     {         
          echo "Error";
          // $_SESSION['messageType'] = 2;
          // header('Location: ../approvedArticle.php?type=3');
     }
 
}
else 
{
     header('Location: ../index.php');
}

?>