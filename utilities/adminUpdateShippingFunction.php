<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $orderId = rewrite($_POST["order_uid"]);
    $shippingMethod = rewrite($_POST["shipping_method"]);
    $trackingNumber = rewrite($_POST["tracking_number"]);
    $deliveredDate = rewrite($_POST["shipping_date"]);

    // $tz = 'Asia/Kuala_Lumpur';
    // $timestamp = time();
    // $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    // $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    // $delivered_on = $dt->format('Y-m-d');

    $shippingStatus = "Delivered";

    // // for debugging
    // echo "<br>";
    // echo $orderId."<br>";
    // echo $shipping_method."<br>";
    // echo $tracking_number."<br>";
    // echo $delivered_on."<br>";
    // echo $orderStatus."<br>";
    // echo $shipping_status."<br>";

    if(isset($_POST['order_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($shippingMethod)
        {
            array_push($tableName,"shipping_method");
            array_push($tableValue,$shippingMethod);
            $stringType .=  "s";
        }     
        if($trackingNumber)
        {
            array_push($tableName,"tracking_number");
            array_push($tableValue,$trackingNumber);
            $stringType .=  "s";
        }     
        if($shippingStatus)
        {
            array_push($tableName,"shipping_status");
            array_push($tableValue,$shippingStatus);
            $stringType .=  "s";
        } 
        if($deliveredDate)
        {
            array_push($tableName,"shipping_date");
            array_push($tableValue,$deliveredDate);
            $stringType .=  "s";
        }
        
        array_push($tableValue,$orderId);
        $stringType .=  "s";
        $orderUpdated = updateDynamicData($conn,"orders"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
        if($orderUpdated)
        {
            header('Location: ../adminOrderTrackingComplete.php');
        }
        else
        {
            echo "fail";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminViewPendingOrders.php?type=2');
        }
    }
    else
    {
        echo "ERROR";
        // $_SESSION['messageType'] = 1;
        // header('Location: ../adminViewPendingOrders.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>