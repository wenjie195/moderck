<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/Level.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// link / level One begin
function linkLevelSalesCommissionOne($conn,$orderUid,$purchaserUid,$purchaserName,$directUplineUid,$directUplineUsername,$directUplineReceiveSalesCommission,$bonusNameOneA)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$directUplineUid,$directUplineUsername,$directUplineReceiveSalesCommission,$bonusNameOneA),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function linkLevelRedemptionPointOne($conn,$orderUid,$purchaserUid,$purchaserName,$directUplineUid,$directUplineUsername,$directUplineReceiveRedemptionPoint,$bonusNameOneB)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$directUplineUid,$directUplineUsername,$directUplineReceiveRedemptionPoint,$bonusNameOneB),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// link / level One end

// link / level Two begin
function linkLevelSalesCommissionTwo($conn,$orderUid,$purchaserUid,$purchaserName,$uplineTwoUid,$uplineTwoUsername,$uplineTwoReceiveSalesCommission,$bonusNameTwoA)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$uplineTwoUid,$uplineTwoUsername,$uplineTwoReceiveSalesCommission,$bonusNameTwoA),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function linkLevelRedemptionPointTwo($conn,$orderUid,$purchaserUid,$purchaserName,$uplineTwoUid,$uplineTwoUsername,$uplineTwoReceiveRedemptionPoint,$bonusNameTwoB)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$uplineTwoUid,$uplineTwoUsername,$uplineTwoReceiveRedemptionPoint,$bonusNameTwoB),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// link / level Two end

// link / level Three begin
function linkLevelSalesCommissionThree($conn,$orderUid,$purchaserUid,$purchaserName,$uplineThreeUid,$uplineThreeUsername,$uplineThreeReceiveSalesCommission,$bonusNameThreeA)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$uplineThreeUid,$uplineThreeUsername,$uplineThreeReceiveSalesCommission,$bonusNameThreeA),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function linkLevelRedemptionPointThree($conn,$orderUid,$purchaserUid,$purchaserName,$uplineThreeUid,$uplineThreeUsername,$uplineThreeReceiveRedemptionPoint,$bonusNameThreeB)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$uplineThreeUid,$uplineThreeUsername,$uplineThreeReceiveRedemptionPoint,$bonusNameThreeB),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// link / level Three end

// link / level Four begin
function linkLevelRedemptionPointFour($conn,$orderUid,$purchaserUid,$purchaserName,$uplineFourUid,$uplineFourUsername,$uplineFourReceiveRedemptionPoint,$bonusNameFourB)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$uplineFourUid,$uplineFourUsername,$uplineFourReceiveRedemptionPoint,$bonusNameFourB),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// link / level Four end

// link / level Five begin
function linkLevelRedemptionPointFive($conn,$orderUid,$purchaserUid,$purchaserName,$uplineFiveUid,$uplineFiveUsername,$uplineFiveReceiveRedemptionPoint,$bonusNameFiveB)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$uplineFiveUid,$uplineFiveUsername,$uplineFiveReceiveRedemptionPoint,$bonusNameFiveB),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// link / level Five end

// link / level Six begin
function linkLevelRedemptionPointSix($conn,$orderUid,$purchaserUid,$purchaserName,$uplineSixUid,$uplineSixUsername,$uplineSixReceiveRedemptionPoint,$bonusNameSixB)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$uplineSixUid,$uplineSixUsername,$uplineSixReceiveRedemptionPoint,$bonusNameSixB),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// link / level Six end

// link / level Seven begin
function linkLevelRedemptionPointSeven($conn,$orderUid,$purchaserUid,$purchaserName,$uplineSevenUid,$uplineSevenUsername,$uplineSevenReceiveRedemptionPoint,$bonusNameSevenB)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$uplineSevenUid,$uplineSevenUsername,$uplineSevenReceiveRedemptionPoint,$bonusNameSevenB),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// link / level Seven end

// first order / rebates begin
function firstOrderBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$firstOrderBonusAmount,$bonusFirstOrder)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$firstOrderBonusAmount,$bonusFirstOrder),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function rebatesBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$rebateBonusAmount,$bonusRebates)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$rebateBonusAmount,$bonusRebates),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function redemptionPointBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$bonusAmount,$bonusRedemptionPoint)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$bonusAmount,$bonusRedemptionPoint),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// first order / rebates end

// direct sponsor begin
function directSponsorMember($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$directBonusMemberAmount,$directBonusMember)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$directBonusMemberAmount,$directBonusMember),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorAgent($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$directBonusAgentAmount,$directBonusAgent)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$directBonusAgentAmount,$directBonusAgent),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// direct sponsor end



if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     echo $orderUid = rewrite($_POST["order_uid"]);
     echo "<br>";  

     $purchaserOrderDetails = getOrders($conn, " WHERE order_id = '$orderUid' ");
     echo $purchaserUid = $purchaserOrderDetails[0]->getUid();
     echo "<br>";
     echo "<br>";
     // echo $orderUserName = $orderDetails[0]->getName();
     // echo "<br>";

     $orderListDetails = getOrderList($conn, " WHERE order_id =? ",array("order_id"),array($orderUid),"s");
     if($orderListDetails)
     {
          $totalProductValue = 0;
          $totalRedemptionPoint = 0;
          for($cnt = 0;$cnt < count($orderListDetails) ;$cnt++)
          {
               echo $orderListDetails[$cnt]->getProductName();
               echo "<br>";
               $totalProductValue += $orderListDetails[$cnt]->getProductValue();
               $totalRedemptionPoint += $orderListDetails[$cnt]->getRedemptionPoint();
          }

          // $totalAmount = 0; // initital
          // for ($cnt=0; $cnt <count($orderListDetails) ; $cnt++)
          // {
          //     $totalAmount += $orderListDetails[$b]->getProductValue();
          // }
     }

     // echo $totalProductValue;
     // echo $totalRedemptionPoint;

     echo "Total Product Value :";
     echo $totalProductValue;
     echo "<br>";
     echo "Total Redemption Point :";
     echo $totalRedemptionPoint;
     echo "<br>";
     echo "<br>";

     $purchaserDetails = getUser($conn, " WHERE uid = ? ",array("uid"),array($purchaserUid),"s");
     echo $purchaserName = $purchaserDetails[0]->getUsername();
     echo "   (";
     echo $purchaserDetails[0]->getLastname();
     echo $purchaserDetails[0]->getFirstname();
     echo ")<br>";
     echo $firstOrder = $purchaserDetails[0]->getFirstOrder();
     echo "<br>";
     echo $purchaserDetails[0]->getRank();
     echo "<br>";
     echo $purchaserSalesCommission = $purchaserDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $purchaserRedemptionPoint = $purchaserDetails[0]->getRedemptionPoint();
     echo "<br>";  

     // user details in referral history
     $userRH = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($purchaserUid),"s");
     echo $userLevel = $userRH[0]->getCurrentLevel();
     echo "<br>";
     echo "<br>";

     //Direct Upline / Level 1
     echo $directUplineUid = $userRH[0]->getReferrerId();
     echo "<br>";  
     $directUplineDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($directUplineUid),"s");
     // echo $directUplineUid = $directUplineDetails[0]->getUid();
     // echo "<br>";  
     echo $directUplineUsername = $directUplineDetails[0]->getUsername();
     echo "<br>";  
     echo $directUplineRank = $directUplineDetails[0]->getRank();
     echo "<br>";  
     echo $directUplineSalesCommission = $directUplineDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $directUplineRedemptionPoint = $directUplineDetails[0]->getRedemptionPoint();

     echo "<br>";  
     echo $directUplineReceiveSalesCommission = 0.10 * ($totalProductValue);
     echo "<br>";  
     echo $directUplineNewSalesCommission = $directUplineReceiveSalesCommission + $directUplineSalesCommission;
     echo "<br>"; 
     echo $directUplineReceiveRedemptionPoint = 0.02 * ($totalRedemptionPoint);
     echo "<br>";  
     echo $directUplineNewRedemptionPoint = $directUplineReceiveRedemptionPoint + $directUplineRedemptionPoint;
     echo "<br>";  

     echo "Direct Sponsor (Commission , Redemption Point)";
     echo "<br>";  
     //direct sponsor bonus begin
     if($directUplineRank == "Agent")
     {
          echo $directUplineDirectSponsorCommission = 0.30 * ($totalProductValue);
          echo "<br>"; 
          echo $directUplineDirectSponsorRedemptionPoint = 0;
     }
     else
     {
          echo $directUplineDirectSponsorCommission = 0.20 * ($totalProductValue);
          echo "<br>"; 
          echo $directUplineDirectSponsorRedemptionPoint = 0.02 * ($totalRedemptionPoint);
     }
     //direct sponsor bonus end

     echo "<br>";  
     echo "<br>";  


     // // first order or rebates begin
     // echo "First Order (Referral Bonus , Rebates)";
     // echo "<br>";  
     // if($firstOrder == 0)
     // {
     //      // referral get bonus
     //      echo "referral get bonus";
     //      echo "<br>";
     //      echo $directUplineCommissionReferralBonus = 0.20 * ($totalProductValue);
     //      echo "<br>"; 
     //      echo $directUplineCommissionRedemption = 0.02 * ($totalRedemptionPoint);
     // }
     // elseif($firstOrder == 1)
     // {
     //      // buyer get rebates  
     //      echo "buyer get rebates";
     //      echo "<br>";
     //      echo $purchaserBonusRebates = 0.20 * ($totalProductValue);
     //      echo "<br>";  
     //      echo $purchaserNewSalesCommission = $purchaserBonusRebates + $purchaserSalesCommission;
     //      echo "<br>"; 
     //      echo $purchaserBonusRedemptionPoint = 0.02 * ($totalRedemptionPoint);
     //      echo "<br>";  
     //      echo $purchaserNewRedemptionPoint = $purchaserBonusRedemptionPoint + $purchaserRedemptionPoint;
     //      echo "<br>";  
     // }
     // else
     // {    }
     // echo "<br>";  
     // echo "<br>";  
     // // first order or rebates end


     //Level 2
     $uplineTwoRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($directUplineUid),"s");
     echo $uplineTwoUid = $uplineTwoRows[0]->getReferrerId();
     echo "<br>";  
     $uplineTwoDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineTwoUid),"s");
     echo $uplineTwoUsername = $uplineTwoDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineTwoRank = $uplineTwoDetails[0]->getRank();
     echo "<br>";  
     echo $uplineTwoSalesCommission = $uplineTwoDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineTwoRedemptionPoint = $uplineTwoDetails[0]->getRedemptionPoint();

     echo "<br>";  
     echo $uplineTwoReceiveSalesCommission = 0.08 * ($totalProductValue);
     echo "<br>";  
     echo $uplineTwoNewSalesCommission = $uplineTwoReceiveSalesCommission + $uplineTwoSalesCommission;
     echo "<br>"; 
     echo $uplineTwoReceiveRedemptionPoint = 0.02 * ($totalRedemptionPoint);
     echo "<br>";  
     echo $uplineTwoNewRedemptionPoint = $uplineTwoReceiveRedemptionPoint + $uplineTwoRedemptionPoint;

     echo "<br>";  
     echo "<br>";

     //Level 3
     $uplineThreeRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineTwoUid),"s");
     echo $uplineThreeUid = $uplineThreeRows[0]->getReferrerId();
     echo "<br>";  
     $uplineThreeDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineThreeUid),"s");
     echo $uplineThreeUsername = $uplineThreeDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineThreeRank = $uplineThreeDetails[0]->getRank();
     echo "<br>";  
     echo $uplineThreeSalesCommission = $uplineThreeDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineThreeRedemptionPoint = $uplineThreeDetails[0]->getRedemptionPoint();

     echo "<br>";  
     echo $uplineThreeReceiveSalesCommission = 0.06 * ($totalProductValue);
     echo "<br>";  
     echo $uplineThreeNewSalesCommission = $uplineThreeReceiveSalesCommission + $uplineThreeSalesCommission;
     echo "<br>"; 
     echo $uplineThreeReceiveRedemptionPoint = 0.02 * ($totalRedemptionPoint);
     echo "<br>";  
     echo $uplineThreeNewRedemptionPoint = $uplineThreeReceiveRedemptionPoint + $uplineThreeRedemptionPoint;

     echo "<br>";  
     echo "<br>";

     //Level 4
     $uplineFourRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineThreeUid),"s");
     echo $uplineFourUid = $uplineFourRows[0]->getReferrerId();
     echo "<br>";  
     $uplineFourDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineFourUid),"s");
     echo $uplineFourUsername = $uplineFourDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineFourRank = $uplineFourDetails[0]->getRank();
     // echo "<br>";  
     // echo $uplineFourSalesCommission = $uplineFourDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineFourRedemptionPoint = $uplineFourDetails[0]->getRedemptionPoint();

     // echo "<br>";  
     // echo $uplineFourNewSalesCommission = 0;
     echo "<br>"; 
     echo $uplineFourReceiveRedemptionPoint = 0.06 * ($totalRedemptionPoint);
     echo "<br>";  
     echo $uplineFourNewRedemptionPoint = $uplineFourReceiveRedemptionPoint + $uplineFourRedemptionPoint;

     echo "<br>";  
     echo "<br>";

     //Level 5
     $uplineFiveRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineFourUid),"s");
     echo $uplineFiveUid = $uplineFiveRows[0]->getReferrerId();
     echo "<br>";  
     $uplineFiveDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineFiveUid),"s");
     echo $uplineFiveUsername = $uplineFiveDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineFiveRank = $uplineFiveDetails[0]->getRank();
     // echo "<br>";  
     // echo $uplineFiveSalesCommission = $uplineFiveDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineFiveRedemptionPoint = $uplineFiveDetails[0]->getRedemptionPoint();

     // echo "<br>";  
     // echo $uplineFiveNewSalesCommission = 0;
     echo "<br>"; 
     echo $uplineFiveReceiveRedemptionPoint = 0.06 * ($totalRedemptionPoint);
     echo "<br>";  
     echo $uplineFiveNewRedemptionPoint = $uplineFiveReceiveRedemptionPoint + $uplineFiveRedemptionPoint;

     echo "<br>";  
     echo "<br>";

     //Level 6
     $uplineSixRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineFiveUid),"s");
     echo $uplineSixUid = $uplineSixRows[0]->getReferrerId();
     echo "<br>";  
     $uplineSixDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineSixUid),"s");
     echo $uplineSixUsername = $uplineSixDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineSixRank = $uplineSixDetails[0]->getRank();
     // echo "<br>";  
     // echo $uplineSixSalesCommission = $uplineSixDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineSixRedemptionPoint = $uplineSixDetails[0]->getRedemptionPoint();

     // echo "<br>";  
     // echo $uplineSixNewSalesCommission = 0;
     echo "<br>"; 
     echo $uplineSixReceiveRedemptionPoint = 0.06 * ($totalRedemptionPoint);
     echo "<br>";  
     echo $uplineSixNewRedemptionPoint = $uplineSixReceiveRedemptionPoint + $uplineSixRedemptionPoint;

     echo "<br>";  
     echo "<br>";

     //Level 7
     $uplineSevenRows = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($uplineSixUid),"s");
     echo $uplineSevenUid = $uplineSevenRows[0]->getReferrerId();
     echo "<br>";  
     $uplineSevenDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($uplineSevenUid),"s");
     echo $uplineSevenUsername = $uplineSevenDetails[0]->getUsername();
     echo "<br>";  
     echo $uplineSevenRank = $uplineSevenDetails[0]->getRank();
     // echo "<br>";  
     // echo $uplineSevenSalesCommission = $uplineSevenDetails[0]->getSalesCommission();
     echo "<br>";  
     echo $uplineSevenRedemptionPoint = $uplineSevenDetails[0]->getRedemptionPoint();

     // echo "<br>";  
     // echo $uplineSevenNewSalesCommission = 0;
     echo "<br>"; 
     echo $uplineSevenReceiveRedemptionPoint = 0.06 * ($totalRedemptionPoint);
     echo "<br>";  
     echo $uplineSevenNewRedemptionPoint = $uplineSevenReceiveRedemptionPoint + $uplineSevenRedemptionPoint;

     echo "<br>";  
     echo "<br>";

     $paymentStatus = 'APPROVED';
     $monthlyBonusStatus = 'PENDING';

     // bonus name begin
     $bonusNameOneA = "10% Sales Commission";
     $bonusNameOneB = "2% Redemption Point";

     $bonusNameTwoA = "8% Sales Commission";
     $bonusNameTwoB = "2% Redemption Point";

     $bonusNameThreeA = "6% Sales Commission";
     $bonusNameThreeB = "2% Redemption Point";

     $bonusNameFourB = "6% Redemption Point";
     $bonusNameFiveB = "6% Redemption Point";
     $bonusNameSixB = "6% Redemption Point";
     $bonusNameSevenB = "6% Redemption Point";

     // $bonusFirstOrder = "20% First Purchase Commission";
     // $bonusRebates = "20% Rebates";
     // $bonusRedemptionPoint = "2% Redemption Point";
     // bonus name end

     // // first order or rebates
     // if($firstOrder == 0)
     // {

     // }
     // else
     // {

     // }

     if($orderListDetails)
     {             
          // level one
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($directUplineNewSalesCommission)
          {
               array_push($tableName,"sales_commission");
               array_push($tableValue,$directUplineNewSalesCommission);
               $stringType .=  "d";
          }
          if($directUplineNewRedemptionPoint)
          {
               array_push($tableName,"redemption_point");
               array_push($tableValue,$directUplineNewRedemptionPoint);
               $stringType .=  "d";
          }

          array_push($tableValue,$directUplineUid);
          $stringType .=  "s";
          $updateLevelOneBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateLevelOneBonus)
          {
               if(linkLevelSalesCommissionOne($conn,$orderUid,$purchaserUid,$purchaserName,$directUplineUid,$directUplineUsername,$directUplineReceiveSalesCommission,$bonusNameOneA))
               {
                    if(linkLevelRedemptionPointOne($conn,$orderUid,$purchaserUid,$purchaserName,$directUplineUid,$directUplineUsername,$directUplineReceiveRedemptionPoint,$bonusNameOneB))
                    {

                         // level two
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         if($uplineTwoNewSalesCommission)
                         {
                              array_push($tableName,"sales_commission");
                              array_push($tableValue,$uplineTwoNewSalesCommission);
                              $stringType .=  "d";
                         }
                         if($uplineTwoNewRedemptionPoint)
                         {
                              array_push($tableName,"redemption_point");
                              array_push($tableValue,$uplineTwoNewRedemptionPoint);
                              $stringType .=  "d";
                         }
               
                         array_push($tableValue,$uplineTwoUid);
                         $stringType .=  "s";
                         $updateLevelTwoBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($updateLevelTwoBonus)
                         {
                              if(linkLevelSalesCommissionTwo($conn,$orderUid,$purchaserUid,$purchaserName,$uplineTwoUid,$uplineTwoUsername,$uplineTwoReceiveSalesCommission,$bonusNameTwoA))
                              {
                                   if(linkLevelRedemptionPointTwo($conn,$orderUid,$purchaserUid,$purchaserName,$uplineTwoUid,$uplineTwoUsername,$uplineTwoReceiveRedemptionPoint,$bonusNameTwoB))
                                   {

                                        // level three
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                        if($uplineThreeNewSalesCommission)
                                        {
                                             array_push($tableName,"sales_commission");
                                             array_push($tableValue,$uplineThreeNewSalesCommission);
                                             $stringType .=  "d";
                                        }
                                        if($uplineThreeNewRedemptionPoint)
                                        {
                                             array_push($tableName,"redemption_point");
                                             array_push($tableValue,$uplineThreeNewRedemptionPoint);
                                             $stringType .=  "d";
                                        }

                                        array_push($tableValue,$uplineThreeUid);
                                        $stringType .=  "s";
                                        $updateLevelThreeBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        if($updateLevelThreeBonus)
                                        {
                                             if(linkLevelSalesCommissionThree($conn,$orderUid,$purchaserUid,$purchaserName,$uplineThreeUid,$uplineThreeUsername,$uplineThreeReceiveSalesCommission,$bonusNameThreeA))
                                             {
                                                  if(linkLevelRedemptionPointThree($conn,$orderUid,$purchaserUid,$purchaserName,$uplineThreeUid,$uplineThreeUsername,$uplineThreeReceiveRedemptionPoint,$bonusNameThreeB))
                                                  {

                                                       // level four
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($uplineFourNewRedemptionPoint)
                                                       {
                                                            array_push($tableName,"redemption_point");
                                                            array_push($tableValue,$uplineFourNewRedemptionPoint);
                                                            $stringType .=  "d";
                                                       }

                                                       array_push($tableValue,$uplineFourUid);
                                                       $stringType .=  "s";
                                                       $updateLevelFourBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($updateLevelFourBonus)
                                                       {
                                                            if(linkLevelRedemptionPointFour($conn,$orderUid,$purchaserUid,$purchaserName,$uplineFourUid,$uplineFourUsername,$uplineFourReceiveRedemptionPoint,$bonusNameFourB))
                                                            {

                                                                 // level five
                                                                 $tableName = array();
                                                                 $tableValue =  array();
                                                                 $stringType =  "";
                                                                 //echo "save to database";
                                                                 if($uplineFiveNewRedemptionPoint)
                                                                 {
                                                                      array_push($tableName,"redemption_point");
                                                                      array_push($tableValue,$uplineFiveNewRedemptionPoint);
                                                                      $stringType .=  "d";
                                                                 }

                                                                 array_push($tableValue,$uplineFiveUid);
                                                                 $stringType .=  "s";
                                                                 $updateLevelFiveBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                 if($updateLevelFiveBonus)
                                                                 {
                                                                      if(linkLevelRedemptionPointFive($conn,$orderUid,$purchaserUid,$purchaserName,$uplineFiveUid,$uplineFiveUsername,$uplineFiveReceiveRedemptionPoint,$bonusNameFiveB))
                                                                      {

                                                                           // level six
                                                                           $tableName = array();
                                                                           $tableValue =  array();
                                                                           $stringType =  "";
                                                                           //echo "save to database";
                                                                           if($uplineSixNewRedemptionPoint)
                                                                           {
                                                                                array_push($tableName,"redemption_point");
                                                                                array_push($tableValue,$uplineSixNewRedemptionPoint);
                                                                                $stringType .=  "d";
                                                                           }

                                                                           array_push($tableValue,$uplineSixUid);
                                                                           $stringType .=  "s";
                                                                           $updateLevelSixBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                           if($updateLevelSixBonus)
                                                                           {
                                                                                if(linkLevelRedemptionPointSix($conn,$orderUid,$purchaserUid,$purchaserName,$uplineSixUid,$uplineSixUsername,$uplineSixReceiveRedemptionPoint,$bonusNameSixB))
                                                                                {

                                                                                     // level seven
                                                                                     $tableName = array();
                                                                                     $tableValue =  array();
                                                                                     $stringType =  "";
                                                                                     //echo "save to database";
                                                                                     if($uplineSevenNewRedemptionPoint)
                                                                                     {
                                                                                          array_push($tableName,"redemption_point");
                                                                                          array_push($tableValue,$uplineSevenNewRedemptionPoint);
                                                                                          $stringType .=  "d";
                                                                                     }

                                                                                     array_push($tableValue,$uplineSevenUid);
                                                                                     $stringType .=  "s";
                                                                                     $updateLevelSevenBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                                     if($updateLevelSevenBonus)
                                                                                     {
                                                                                          if(linkLevelRedemptionPointSeven($conn,$orderUid,$purchaserUid,$purchaserName,$uplineSevenUid,$uplineSevenUsername,$uplineSevenReceiveRedemptionPoint,$bonusNameSevenB))
                                                                                          {
                                                                                               echo "Link / Level All Success";
                                                                                               echo "<br>"; 

                                                                                               if($firstOrder == 0)
                                                                                               {
                                                                                                    echo $firstPurchse = 0.20 * ($totalProductValue);
                                                                                                    echo "<br>"; 
                                                                                                    echo $firstOrderBonusRedemptionPoint = 0.02 * ($totalRedemptionPoint);
                                                                                                    echo "<br>"; 
                                                                                                    echo $directUplineFirstOrderSalesCommission = $firstPurchse + $directUplineNewSalesCommission;
                                                                                                    echo "<br>"; 
                                                                                                    echo $directUplineFirstOrderRedemptionPoint = $directUplineNewRedemptionPoint + $firstOrderBonusRedemptionPoint;
                                                                                                    echo "<br>"; 

                                                                                                    // First Order Bonus
                                                                                                    $tableName = array();
                                                                                                    $tableValue =  array();
                                                                                                    $stringType =  "";
                                                                                                    //echo "save to database";
                                                                                                    if($directUplineFirstOrderSalesCommission)
                                                                                                    {
                                                                                                         array_push($tableName,"sales_commission");
                                                                                                         array_push($tableValue,$directUplineFirstOrderSalesCommission);
                                                                                                         $stringType .=  "d";
                                                                                                    }
                                                                                                    if($directUplineFirstOrderRedemptionPoint)
                                                                                                    {
                                                                                                         array_push($tableName,"redemption_point");
                                                                                                         array_push($tableValue,$directUplineFirstOrderRedemptionPoint);
                                                                                                         $stringType .=  "d";
                                                                                                    }

                                                                                                    array_push($tableValue,$directUplineUid);
                                                                                                    $stringType .=  "s";
                                                                                                    $updateFirstOrderBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                                                    if($updateFirstOrderBonus)
                                                                                                    {

                                                                                                         // bonus name begin
                                                                                                         $bonusFirstOrder = "20% First Purchase Commission";
                                                                                                         $bonusRedemptionPoint = "2% Redemption Point";

                                                                                                         $firstOrderBonusAmount = $firstPurchse;
                                                                                                         $bonusAmount = $firstOrderBonusRedemptionPoint;
                                                                                                         $receiverUid = $directUplineUid;
                                                                                                         $receiverUsername = $directUplineUsername;

                                                                                                         $updateFirstPurchaseStatus = '1';
                                                                                                         // bonus name end

                                                                                                         // Update First Order Status 
                                                                                                         $tableName = array();
                                                                                                         $tableValue =  array();
                                                                                                         $stringType =  "";
                                                                                                         //echo "save to database";
                                                                                                         if($updateFirstPurchaseStatus)
                                                                                                         {
                                                                                                              array_push($tableName,"first_order");
                                                                                                              array_push($tableValue,$updateFirstPurchaseStatus);
                                                                                                              $stringType .=  "s";
                                                                                                         }
                                                                                                         array_push($tableValue,$purchaserUid);
                                                                                                         $stringType .=  "s";
                                                                                                         $updateFirstOrderStat = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                                                         if($updateFirstOrderStat)
                                                                                                         {

                                                                                                              if(firstOrderBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$firstOrderBonusAmount,$bonusFirstOrder))
                                                                                                              {
                                                                                                                   if(redemptionPointBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$bonusAmount,$bonusRedemptionPoint))
                                                                                                                   {
     
                                                                                                                   }
                                                                                                                   else
                                                                                                                   {
                                                                                                                        echo "Fail Redemption Bonus";
                                                                                                                   }
                                                                                                              }
                                                                                                              else
                                                                                                              {
                                                                                                                   echo "Fail First Order Bonus";
                                                                                                              }

                                                                                                         }
                                                                                                         else
                                                                                                         {
                                                                                                              echo "Fail First Order Status";
                                                                                                         }

                                                                                                         // if(firstOrderBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$firstOrderBonusAmount,$bonusFirstOrder))
                                                                                                         // {
                                                                                                         //      if(redemptionPointBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$bonusAmount,$bonusRedemptionPoint))
                                                                                                         //      {

                                                                                                         //      }
                                                                                                         //      else
                                                                                                         //      {
                                                                                                         //           echo "Fail Redemption Bonus";
                                                                                                         //      }
                                                                                                         // }
                                                                                                         // else
                                                                                                         // {
                                                                                                         //      echo "Fail First Order Bonus";
                                                                                                         // }

                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                         echo "Error First Order Bonus";
                                                                                                    }

                                                                                               }
                                                                                               elseif($firstOrder == 1)
                                                                                               {
                                                                                                    echo $rebates = 0.20 * ($totalProductValue);
                                                                                                    echo "<br>"; 
                                                                                                    echo $rebatesBonusRedemptionPoint = 0.02 * ($totalRedemptionPoint);
                                                                                                    echo "<br>"; 
                                                                                                    echo $purchaserRebatesSalesCommission = $rebates + $purchaserSalesCommission;
                                                                                                    echo "<br>"; 
                                                                                                    echo $purchaserRebatesRedemptionPoint = $rebatesBonusRedemptionPoint + $purchaserRedemptionPoint;
                                                                                                    echo "<br>"; 

                                                                                                    // Purchaser Rebates
                                                                                                    $tableName = array();
                                                                                                    $tableValue =  array();
                                                                                                    $stringType =  "";
                                                                                                    //echo "save to database";
                                                                                                    if($purchaserRebatesSalesCommission)
                                                                                                    {
                                                                                                         array_push($tableName,"sales_commission");
                                                                                                         array_push($tableValue,$purchaserRebatesSalesCommission);
                                                                                                         $stringType .=  "d";
                                                                                                    }
                                                                                                    if($purchaserRebatesRedemptionPoint)
                                                                                                    {
                                                                                                         array_push($tableName,"redemption_point");
                                                                                                         array_push($tableValue,$purchaserRebatesRedemptionPoint);
                                                                                                         $stringType .=  "d";
                                                                                                    }

                                                                                                    array_push($tableValue,$purchaserUid);
                                                                                                    $stringType .=  "s";
                                                                                                    $updatePurchaserRebates = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                                                    if($updatePurchaserRebates)
                                                                                                    {

                                                                                                         // bonus name begin
                                                                                                         $bonusRebates = "20% Rebates";
                                                                                                         $bonusRedemptionPoint = "2% Redemption Point";

                                                                                                         $rebateBonusAmount = $rebates;
                                                                                                         $bonusAmount = $rebatesBonusRedemptionPoint;
                                                                                                         $receiverUid = $purchaserUid;
                                                                                                         $receiverUsername = $purchaserName;
                                                                                                         // bonus name end

                                                                                                         if(rebatesBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$rebateBonusAmount,$bonusRebates))
                                                                                                         {
                                                                                                              if(redemptionPointBonus($conn,$orderUid,$purchaserUid,$purchaserName,$receiverUid,$receiverUsername,$bonusAmount,$bonusRedemptionPoint))
                                                                                                              {

                                                                                                              }
                                                                                                              else
                                                                                                              {
                                                                                                                   echo "Fail Redemption Bonus";
                                                                                                              }
                                                                                                         }
                                                                                                         else
                                                                                                         {
                                                                                                              echo "Fail Rebate Bonus";
                                                                                                         }

                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                         echo "Error Purchaser Rebates";
                                                                                                    }

                                                                                               }

                                                                                          }
                                                                                          else
                                                                                          {
                                                                                               echo "Error Redemption Point 7";
                                                                                          }
                                                                                     }
                                                                                     else
                                                                                     {
                                                                                          echo "Fail Level 7";
                                                                                     }

                                                                                }
                                                                                else
                                                                                {
                                                                                     echo "Error Redemption Point 6";
                                                                                }
                                                                           }
                                                                           else
                                                                           {
                                                                                echo "Fail Level 6";
                                                                           }

                                                                      }
                                                                      else
                                                                      {
                                                                           echo "Error Redemption Point 5";
                                                                      }
                                                                 }
                                                                 else
                                                                 {
                                                                      echo "Fail Level 5";
                                                                 }

                                                            }
                                                            else
                                                            {
                                                                 echo "Error Redemption Point 4";
                                                            }
                                                       }
                                                       else
                                                       {
                                                            echo "Fail Level 4";
                                                       }

                                                  }
                                                  else
                                                  {
                                                       echo "Error Redemption Point 3";
                                                  }
                                             }
                                             else
                                             {
                                                  echo "Error Commission 3";
                                             }
                                        }
                                        else
                                        {
                                             echo "Fail Level 3";
                                        }

                                   }
                                   else
                                   {
                                        echo "Error Redemption Point 2";
                                   }
                              }
                              else
                              {
                                   echo "Error Commission 2";
                              }
                         }
                         else
                         {
                              echo "Fail Level 2";
                         }

                    }
                    else
                    {
                         echo "Error Redemption Point 1";
                    }
               }
               else
               {
                    echo "Error Commission 1";
               }
          }
          else
          {
               echo "Fail Level 1";
          }
     }
     else
     {
          echo "Error 404";
     }

}
else 
{
    header('Location: ../index.php');
}
?>