<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);

     $status = "Delete";

     $productDetails = getProduct($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($productDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }

          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $announcementUpdated = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($announcementUpdated)
          {
               // echo "Product Deleted !";
               header('Location: ../adminProductAll.php');
          }
          else
          {
               echo "Fail";
          }
     }
     else
     {
          echo "Error";
     }

}
else
{
     header('Location: ../index.php');
}
?>