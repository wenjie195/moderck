<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $withdrawalUid = rewrite($_POST["item_uid"]);

     $method = rewrite($_POST["payment_method"]);
     $reference = rewrite($_POST["reference"]);
     $transactionDate = rewrite($_POST["transaction_date"]);

     $status = "APPROVED";

     // //   FOR DEBUGGING
     // echo "<br>";
     // // echo $approvedStatus."<br>";
     // // echo $rejectedStatus."<br>";

     $itemDetails = getWithdrawal($conn," WHERE withdrawal_uid = ? ",array("withdrawal_uid"),array($withdrawalUid),"s");   

     if($itemDetails)
     {  
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($method)
          {
               array_push($tableName,"method");
               array_push($tableValue,$method);
               $stringType .=  "s";
          }
          if($reference)
          {
               array_push($tableName,"receipt");
               array_push($tableValue,$reference);
               $stringType .=  "s";
          }
          if($transactionDate)
          {
               array_push($tableName,"note");
               array_push($tableValue,$transactionDate);
               $stringType .=  "s";
          }

          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }

          array_push($tableValue,$withdrawalUid);
          $stringType .=  "s";
          $announcementUpdated = updateDynamicData($conn,"withdrawal"," WHERE withdrawal_uid = ? ",$tableName,$tableValue,$stringType);
          if($announcementUpdated)
          {
               // echo "Success !";
               header('Location: ../adminWithdrawalCompleted.php');
          }
          else
          {
               echo "Fail !";
          }
     }
     else
     {    echo "ERROR !"; }
}
else
{
     header('Location: ../index.php');
}
?>