<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $username = rewrite($_POST['update_username']);
    $fullname = rewrite($_POST["update_fullname"]);
    $icNumber = rewrite($_POST["update_icno"]);
    $email = rewrite($_POST["update_email"]);
    $phone = rewrite($_POST["update_phone"]);


    $bankName = rewrite($_POST["update_bank"]);
    $bankAccountHolder = rewrite($_POST["update_bank_holder"]);
    $bankAccountNumber = rewrite($_POST["update_bank_account"]);

    $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");    

    // $icRows = getUser($conn," WHERE icno = ? ",array("icno"),array($icNumber),"s");
    // $existingIcNumber = $icRows[0];
    // $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
    // $existingUsername = $usernameRows[0];

    if ($user)
    {

        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        if($fullname)
        {
            array_push($tableName,"fullname");
            array_push($tableValue,$fullname);
            $stringType .=  "s";
        }
        if($icNumber)
        {
            array_push($tableName,"icno");
            array_push($tableValue,$icNumber);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }

        // if($bankName)
        if($bankName || !$bankName)
        {
            array_push($tableName,"bank_name");
            array_push($tableValue,$bankName);
            $stringType .=  "s";
        }
        if($bankAccountHolder || !$bankAccountHolder)
        {
            array_push($tableName,"bank_acc_name");
            array_push($tableValue,$bankAccountHolder);
            $stringType .=  "s";
        }
        if($bankAccountNumber || !$bankAccountNumber)
        {
            array_push($tableName,"bank_acc_number");
            array_push($tableValue,$bankAccountNumber);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../userProfile.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../userProfile.php?type=3');
        }

    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../userProfile.php?type=4');
    }

}
else 
{
    header('Location: ../index.php');
}
?>