<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Address.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

function registerAddress($conn,$uid,$userUid,$recipient,$mobile,$address,$city,$state,$postcode,$country,$defaultShip,$defaultBill,$status)
{
     if(insertDynamicData($conn,"address",array("uid","user_uid","recipient","mobile","house_road","city","state","postcode","country","default_ship","default_bill","status"),
          array($uid,$userUid,$recipient,$mobile,$address,$city,$state,$postcode,$country,$defaultShip,$defaultBill,$status),"ssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $recipient = rewrite($_POST['recipient_name']);
     $mobile = rewrite($_POST['mobile_no']);
     $houseRoad = rewrite($_POST['house_road']);
     $postcode = rewrite($_POST['postcode']);
     $city = rewrite($_POST['city']);
     $state = rewrite($_POST['state']);
     $country = rewrite($_POST['country']);
     $status = "Available";

     $defaultShip = rewrite($_POST['default_ship']);
     $defaultBill = rewrite($_POST['default_bill']);
    
     if($defaultShip == "Yes")
     {
          if($defaultBill == "Yes")
          {
     
               $allUserAddress = getAddress($conn, "WHERE user_uid = ? ", array("user_uid"), array($userUid), "s");
               if($allUserAddress)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
     
                    $updateDefaultShipStatus = "No";
                    $updateDefaultBillStatus = "No";
     
                    if($updateDefaultShipStatus)
                    {
                         array_push($tableName,"default_ship");
                         array_push($tableValue,$updateDefaultShipStatus);
                         $stringType .=  "s";
                    }  
                    if($updateDefaultBillStatus)
                    {
                         array_push($tableName,"default_bill");
                         array_push($tableValue,$updateDefaultBillStatus);
                         $stringType .=  "s";
                    }  
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    $updateAddressBothYes = updateDynamicData($conn,"address"," WHERE user_uid = ? AND status = 'Available' ",$tableName,$tableValue,$stringType);
                    if($updateAddressBothYes)
                    {
                         if(registerAddress($conn,$uid,$userUid,$recipient,$mobile,$houseRoad,$city,$state,$postcode,$country,$defaultShip,$defaultBill,$status))
                         {
                              header('Location: ../userAddressBook.php');
                         }
                         else
                         {
                              echo "Error 1(A)";
                         }
                    }
                    else
                    {
                         echo "Error 1(A1)";
                         echo "<br>";
                    }
               }

          }
          elseif($defaultBill == "No")
          {
     
               $allUserAddress = getAddress($conn, "WHERE user_uid = ? ", array("user_uid"), array($userUid), "s");
               if($allUserAddress)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
     
                    $updateDefaultShipStatus = "No";
                    // $updateDefaultBillStatus = "No";
     
                    if($updateDefaultShipStatus)
                    {
                         array_push($tableName,"default_ship");
                         array_push($tableValue,$updateDefaultShipStatus);
                         $stringType .=  "s";
                    }  
                    // if($updateDefaultBillStatus)
                    // {
                    //      array_push($tableName,"default_bill");
                    //      array_push($tableValue,$updateDefaultBillStatus);
                    //      $stringType .=  "s";
                    // }  
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    $updateAddressYesNo = updateDynamicData($conn,"address"," WHERE user_uid = ? AND status = 'Available' ",$tableName,$tableValue,$stringType);
                    if($updateAddressYesNo)
                    {
                         if(registerAddress($conn,$uid,$userUid,$recipient,$mobile,$houseRoad,$city,$state,$postcode,$country,$defaultShip,$defaultBill,$status))
                         {
                              header('Location: ../userAddressBook.php');
                         }
                         else
                         {
                              echo "Error 1(B)";
                         }
                    }
                    else
                    {
                         echo "Error 1(B1)";
                         echo "<br>";
                    }
               }

          }
     }
     elseif($defaultShip == "No")
     {
          if($defaultBill == "Yes")
          {

               $allUserAddress = getAddress($conn, "WHERE user_uid = ? ", array("user_uid"), array($userUid), "s");
               if($allUserAddress)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
     
                    // $updateDefaultShipStatus = "No";
                    $updateDefaultBillStatus = "No";
     
                    // if($updateDefaultShipStatus)
                    // {
                    //      array_push($tableName,"default_ship");
                    //      array_push($tableValue,$updateDefaultShipStatus);
                    //      $stringType .=  "s";
                    // }  
                    if($updateDefaultBillStatus)
                    {
                         array_push($tableName,"default_bill");
                         array_push($tableValue,$updateDefaultBillStatus);
                         $stringType .=  "s";
                    }  
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    $updateAddressNoYes = updateDynamicData($conn,"address"," WHERE user_uid = ? AND status = 'Available' ",$tableName,$tableValue,$stringType);
                    if($updateAddressNoYes)
                    {
                         if(registerAddress($conn,$uid,$userUid,$recipient,$mobile,$houseRoad,$city,$state,$postcode,$country,$defaultShip,$defaultBill,$status))
                         {
                              header('Location: ../userAddressBook.php');
                         }
                         else
                         {
                              echo "Error 2(A)";
                         }
                    }
                    else
                    {
                         echo "Error 2(A1)";
                         echo "<br>";
                    }
               }

          }
          elseif($defaultBill == "No")
          {
               if(registerAddress($conn,$uid,$userUid,$recipient,$mobile,$houseRoad,$city,$state,$postcode,$country,$defaultShip,$defaultBill,$status))
               {
                    header('Location: ../userAddressBook.php');
               }
               else
               {
                    echo "Error 2(B)";
               }
          }
     }
     else
     {
          echo "Error (Yes / No)";
     }
}
else 
{
     header('Location: ../index.php');
}
?>