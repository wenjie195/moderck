<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allOrders = getOrders($conn);
$allOrders = getOrders($conn, " WHERE payment_status = 'PENDING' ");
// $allOrders = getOrders($conn, " WHERE uid = ? AND status = 'Pending' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Purchase Listing | MODERCK" />
<title>Purchase Listing | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<?php
// header("refresh: 1");
?>

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Purchase Listing</h1><?php include 'header.php'; ?>
</div>

<!-- <?php //unset($_SESSION['order_uid']);?> -->

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 scroll-div">
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>DATE</th>
                            <th>REF NO</th>
                            <th>USERNAME</th>
                            <th>TOTAL AMOUNT (RM)</th>
                            <th>PAYMENT REFERENCE</th>
                            <th>PAYMENT STATUS</th>
                            <th>DELIVERY STATUS</th>
                            <th>DETAILS</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($allOrders)
                            {
                                for($cnt = 0;$cnt < count($allOrders) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($allOrders[$cnt]->getDateCreated()));?>
                                        </td>

                                        <!-- <td>INV</td> -->
                                        <td>
                                            INV <?php 
                                                $string = $allOrders[$cnt]->getOrderId();
                                                echo substr($string, -8) ;
                                            ?>
                                        </td>

                                        <!-- <td><?php //echo $allOrders[$cnt]->getName();?></td> -->

                                        <td>
                                            <?php 
                                                $userUid = $allOrders[$cnt]->getUid();
                                                $conn = connDB();
                                                $userDetails = getUser($conn, " WHERE uid = ? ",array("uid"),array($userUid),"s");
                                                echo $userDetails[0]->getUsername();
                                                $conn->close();
                                            ?>
                                        </td>

                                        <td><?php echo $allOrders[$cnt]->getSubtotal();?></td>
                                        <td><?php echo $allOrders[$cnt]->getPaymentReference();?></td>
                                        <td><?php echo $allOrders[$cnt]->getPaymentStatus();?></td>
                                        <td><?php echo $allOrders[$cnt]->getShippingStatus();?></td>

                                        <td>
                                            <!-- <form> -->
                                            <form method="POST" action="adminSalesPurchaseListingDetails.php">
                                                <button class="clean dark-tur-link view-link" type="submit" name="order_id" value="<?php echo $allOrders[$cnt]->getOrderId();?>">
                                                    <u>View</u>
                                                </button>
                                            </form>
                                        </td>

                                        <td>
                                            <!-- <form action="#" method="POST" class="left-form"> -->
                                            <!-- <form action="utilities/adminOrderApprovedFunction.php" method="POST" class="left-form"> -->
                                            <!-- <form action="utilities/adminOrderApprovedRenewFunction.php" method="POST" class="left-form"> -->
                                            <!-- <form action="utilities/adminOrderApprovedPreLoadFunction.php" method="POST" class="left-form"> -->
                                            <form action="utilities/adminOrderApprovedPreLoadRenewFunction.php" method="POST" class="left-form">
                                            <!-- <form action="utilities/adminOrderApprovedClearFirstOrderFunction.php" method="POST" class="left-form"> -->
                                                <button class="clean transparent-button white-link" type="submit" name="order_uid" value="<?php echo $allOrders[$cnt]->getOrderId();?>">
                                                    <u>Approve</u>
                                                </button>
                                            </form> 

                                            <form action="#" method="POST" class="right-form">
                                                <button class="clean transparent-button red-link2" type="submit" name="order_uid" value="<?php echo $allOrders[$cnt]->getOrderId();?>">
                                                    <u>Reject</u>
                                                </button>
                                            </form> 
                                        </td>

                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>