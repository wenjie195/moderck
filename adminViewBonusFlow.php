<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Bonus Flow Report | MODERCK" />
<title>Bonus Flow Report | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Bonus Flow Report</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 scroll-div">
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>BONUS RECEIVER</th>
                            <th>AMOUNT</th>
                            <th>BONUS TYPE</th>
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if(isset($_POST['order_id']))
                        {
                        $conn = connDB();
                        $bonusDetails = getBonus($conn," WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_id']),"s");

                            if($bonusDetails)
                            {
                                for($cnt = 0;$cnt < count($bonusDetails) ;$cnt++)
                                {
                                ?>
                                
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $bonusDetails[$cnt]->getReceiver();?></td>
                                        <td><?php echo $bonusDetails[$cnt]->getAmount();?></td>
                                        <td><?php echo $bonusDetails[$cnt]->getBonusType();?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($bonusDetails[$cnt]->getDateCreated()));?>
                                        </td>
                                    </tr>

                                <?php
                                }
                            }
                        }
                        ?>
                                
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>