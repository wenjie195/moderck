<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allOrders = getOrders($conn);
$allOrders = getOrders($conn, " WHERE payment_status = 'Approved' AND shipping_status = 'Pending' ORDER BY date_created DESC ");
// $allOrders = getOrders($conn, " WHERE uid = ? AND status = 'Pending' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Shipping (Pending) | MODERCK" />
<title>Shipping (Pending) | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Order Tracking (Pending) | <a href="adminOrderTrackingComplete.php" class="color-a">Order Tracking (Completed)</a></h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 scroll-div">
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>DATE</th>
                            <th>REF NO</th>
                            <th>USERNAME</th>
                            <th>TOTAL AMOUNT (RM)</th>
                            <th>PAYMENT REFERENCE</th>
                            <th>PAYMENT STATUS</th>
                            <th>DELIVERY STATUS</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($allOrders)
                            {
                                for($cnt = 0;$cnt < count($allOrders) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($allOrders[$cnt]->getDateCreated()));?>
                                        </td>

                                        <!-- <td>INV</td> -->
                                        <td>
                                            INV <?php 
                                                $string = $allOrders[$cnt]->getOrderId();
                                                echo substr($string, -8) ;
                                            ?>
                                        </td>

                                        <td><?php echo $allOrders[$cnt]->getName();?></td>
                                        <td><?php echo $allOrders[$cnt]->getSubtotal();?></td>
                                        <td><?php echo $allOrders[$cnt]->getPaymentReference();?></td>
                                        <td><?php echo $allOrders[$cnt]->getPaymentStatus();?></td>
                                        <td><?php echo $allOrders[$cnt]->getShippingStatus();?></td>

                                        <td>
                                            <!-- <form> -->
                                            <form method="POST" action="adminOrderTrackingDetails.php">
                                                <button class="clean dark-tur-link view-link" type="submit" name="order_id" value="<?php echo $allOrders[$cnt]->getOrderId();?>">
                                                    <u>Update</u>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>