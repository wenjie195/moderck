<?php
if(isset($_SESSION['uid']))
{
  ?>
  <?php
  if($_SESSION['user_type'] == 0)
  //admin
  {
  ?>

    <div class="page-wrapper">
      <a id="show-sidebar" >
        <img src="img/menu.png" class="menu-icon opacity-hover pointer" alt="Open Menu" title="Open Menu">
      </a>
      <nav id="sidebar" class="sidebar-wrapper">
        <div class="sidebar-content">
          <div class="sidebar-brand">
            <div id="close-sidebar">
              <img src="img/close.png" class="menu-icon opacity-hover pointer close-icon" alt="Close Menu" title="Close Menu">
            </div>
          </div>

          <div class="sidebar-menu">
            <ul>
              <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
                <p class="margin-0 swing-hover">
                  <!-- <span><img src="img/more.png" class="more-icon swing"></span> -->
                  <a href="adminDashboard.php" class="width-auto margin-top-3"><span class="bold-menu white-to-tur">DASHBOARD</span></a>
                </p>
                
                <!-- <div class="sidebar-submenu">
                  <ul>
                    <li>
                      <a href="#" class="white-to-tur">Profile</a>
                    </li>
                  </ul>
                </div>           -->
              </li>                      
              
              <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
                <p class="margin-0 swing-hover">
                  <span><img src="img/more.png" class="more-icon swing"></span>
                  <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">PROFILE</span></a>
                </p>
                
                <div class="sidebar-submenu">
                  <ul>
                    <li>
                      <a href="#" class="white-to-tur">Profile</a>
                    </li>
                    <li>
                      <a href="#" class="white-to-tur">Address Book</a>
                    </li>
                  </ul>
                </div>          
              </li>    

              <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
                <p class="margin-0 swing-hover">
                  <span><img src="img/more.png" class="more-icon swing"></span>
                  <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">CHAIN NET</span></a>
                </p>
                
                <div class="sidebar-submenu">
                  <ul>
                    <!-- <li>
                      <a href="#" class="white-to-tur">Add New User</a>
                    </li> -->
                    <li>
                      <a href="adminMemberOverview.php" class="white-to-tur">Overview</a>
                    </li>
                    <li>
                      <a href="adminChainNet.php" class="white-to-tur">Chain-Net</a>
                    </li>
                  </ul>
                </div>          
              </li>    

              <!-- <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
                <p class="margin-0 swing-hover">
                  <span><img src="img/more.png" class="more-icon swing"></span>
                  <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">NETWORK</span></a>
                </p>
                
                <div class="sidebar-submenu">
                  <ul>
                    <li>
                      <a href="#" class="white-to-tur">New Registration</a>
                    </li>
                    <li>
                      <a href="adminMemberOverview.php" class="white-to-tur">Member Overview</a>
                    </li>
                    <li>
                      <a href="#" class="white-to-tur">Network Tree</a>
                    </li>
                  </ul>
                </div>          
              </li>     -->
              
              <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
                <p class="margin-0 swing-hover">
                  <span><img src="img/more.png" class="more-icon swing"></span>
                  <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">SALES</span></a>
                </p>
                
                <div class="sidebar-submenu">
                  <ul>
                    <li>
                      <a href="#" class="white-to-tur">Shopping Cart</a>
                    </li>
                    <li>
                      <a href="adminOrderTrackingPending.php" class="white-to-tur">Order Tracking</a>
                    </li>
                    <li>
                      <a href="adminSalesPurchaseListing.php" class="white-to-tur">Purchase History</a>
                    </li>
                    <!-- <li>
                      <a href="#" class="white-to-tur">Cart Listing</a>
                    </li> -->
                    <li>
                      <a href="adminSalesReport.php" class="white-to-tur">Sales Report</a>
                    </li>
                  </ul>
                </div>          
              </li>   

              <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
                <p class="margin-0 swing-hover">
                  <span><img src="img/more.png" class="more-icon swing"></span>
                  <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">TRANSFER</span></a>
                </p>
                
                <div class="sidebar-submenu">
                  <ul>
                    <li>
                      <a href="#" class="white-to-tur">Transfer Station</a>
                    </li>
                    <li>
                      <a href="#" class="white-to-tur">Transfer History</a>
                    </li>
                    <li>
                      <a href="#" class="white-to-tur">Transfer Report</a>
                    </li>
                  </ul>
                </div>          
              </li>   

              <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
                <p class="margin-0 swing-hover">
                  <span><img src="img/more.png" class="more-icon swing"></span>
                  <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">PAYOUT</span></a>
                </p>
                
                <div class="sidebar-submenu">
                  <ul>
                    <!-- <li>
                      <a href="#" class="white-to-tur">Payout Listing / Account Listing</a>
                    </li> -->
                    <li>
                      <a href="adminReportPayout.php" class="white-to-tur">Payout History</a>
                    </li>
                    <li>
                      <a href="adminReportPayout.php" class="white-to-tur">Payout Report</a>
                    </li>
                    <li>
                      <a href="#" class="white-to-tur">Overall Report</a>
                    </li>

                    <li>
                      <a href="adminWithdrawalPending.php" class="white-to-tur">Withdrawal</a>
                    </li>

                  </ul>
                </div>          
              </li>   

              <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
                <p class="margin-0 swing-hover">
                  <span><img src="img/more.png" class="more-icon swing"></span>
                  <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">PRODUCT</span></a>
                </p>
                
                <div class="sidebar-submenu">
                  <ul>
                    <li>
                      <a href="adminProductAll.php" class="white-to-tur">Product Management</a>
                    </li>
                    <!-- <li>
                      <a href="#" class="white-to-tur">Payout Report</a>
                    </li> -->
                  </ul>
                </div>          
              </li>   

              <li class="sidebar-dropdown padding-tb10 pointer">
                  <a href="logout.php"><span class="bold-menu white-to-tur extra-margin-left">Logout</span></a>
              </li>          
              
            </ul>
          </div>

        </div>
      </nav>
    </div>

  <?php
  }
  elseif($_SESSION['user_type'] == 1)
  //user
  {
  ?>

  <div class="page-wrapper">
    <a id="show-sidebar" >
      <img src="img/menu.png" class="menu-icon opacity-hover pointer" alt="Open Menu" title="Open Menu">
    </a>
    <nav id="sidebar" class="sidebar-wrapper">
      <div class="sidebar-content">
        <div class="sidebar-brand">
          <div id="close-sidebar">
            <img src="img/close.png" class="menu-icon opacity-hover pointer close-icon" alt="Close Menu" title="Close Menu">
          </div>
        </div>

        <div class="sidebar-menu">
          <ul>
            <li class="sidebar-dropdown padding-tb10 pointer">
                <a href="userDashboard.php"><span class="bold-menu white-to-tur extra-margin-left">DASHBOARD</span></a>
            </li>   

            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">Profile</span></a>
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="userProfile.php" class="white-to-tur">Profile</a>
                  </li>
                  <li>
                    <a href="userAddressBook.php" class="white-to-tur">Address Book</a>
                  </li>
                </ul>
              </div>
            </li>
            
           <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">SALES</span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="eCommerceSite.php" class="white-to-tur">eCommerce Site</a>
                  </li>
                  <li>
                    <a href="shoppingCart.php" class="white-to-tur">Shopping Cart</a>
                  </li>
                  <li>
                    <a href="#.php" class="white-to-tur">Order Tracking</a>
                  </li>
                  <li>
                    <a href="userPurchaseHistory.php" class="white-to-tur">Purchase History</a>
                  </li>   
                </ul>
              </div>
            </li>         
            
            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur">USER</span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="userChainNet.php" class="white-to-tur">Chain-Net</a>
                  </li>              
                  <li>
                    <a href="#.php" class="white-to-tur">Transfer</a>
                  </li>
                  <li>
                    <a href="userCommissionHistory.php" class="white-to-tur">Commission History</a>
                  </li> 
                  <li>
                    <a href="#.php" class="white-to-tur">Overall History</a>
                  </li>              
                  
                  <li>
                    <a href="userWithdrawal.php" class="white-to-tur">Withdrawal</a>
                  </li>    
                  <li>
                    <a href="userWithdrawalHistory.php" class="white-to-tur">Withdrawal History</a>
                  </li>    

                </ul>
              </div>
            </li>     
           
            <li class="sidebar-dropdown padding-tb10 pointer">
                <a href="logout.php"><span class="bold-menu white-to-tur extra-margin-left">Logout</span></a>
            </li>          
            
          </ul>
        </div>

      </div>
    </nav>
  </div>

  <?php
  }
  ?>
  <?php
}
else
{ }
?>