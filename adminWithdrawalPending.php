<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allWithdrawal = getWithdrawal($conn, " ORDER BY date_created DESC ");
$allWithdrawal = getWithdrawal($conn, " WHERE status = 'PENDING' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Withdrawal History | MODERCK" />
<title>Withdrawal History | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Withdrawal (Pending) | <a href="adminWithdrawalCompleted.php" class="color-a">Withdrawal (Completed)</a></h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 scroll-div">
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>DATE</th>
                            <th>REF NO</th>
                            <th>AMOUNT (RM)</th>

                            <th>BANK NAME</th>
                            <th>BANK ACCOUNT HOLDER</th>
                            <th>BANK ACCOUNT NUMBER</th>

                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($allWithdrawal)
                            {
                                for($cnt = 0;$cnt < count($allWithdrawal) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($allWithdrawal[$cnt]->getDateCreated()));?>
                                        </td>

                                        <!-- <td>INV</td> -->
                                        <td>
                                            REF <?php 
                                                $string = $allWithdrawal[$cnt]->getWithdrawalUid();
                                                echo substr($string, -8) ;
                                            ?>
                                        </td>

                                        <td><?php echo $allWithdrawal[$cnt]->getAmount();?></td>
                                        <td><?php echo $allWithdrawal[$cnt]->getBankName();?></td>
                                        <td><?php echo $allWithdrawal[$cnt]->getBankAccHolder();?></td>
                                        <td><?php echo $allWithdrawal[$cnt]->getBankAccNo();?></td>

                                        <td>
                                            <form method="POST" action="adminWithdrawalDetails.php">
                                                <button class="clean dark-tur-link view-link" type="submit" name="item_uid" value="<?php echo $allWithdrawal[$cnt]->getWithdrawalUid();?>">
                                                    <u>Update</u>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>