<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Livestream.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allLivestream = getLivestream($conn);
// $allLivestream = getLivestream($conn, "WHERE status != 'Delete' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="ALL Livestream | MODERCK" />
<title>ALL Livestream | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">All Livestream</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div padding-bottom30">
    		<div class="text-center middle-div-width">
            	<a href="adminLivestreamAdd.php"><div  class="clean white-button ow-red-bg white-text smaller-button">Add</div></a> 
            </div> 
             <div class="width100 scroll-div">
            <table class="width100 gold-table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>USERNAME</th>
                        <th>CHANNEL</th>
                        <th>DATE</th>
                        <th>TIME START</th>
                        <th>TIME END</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($allLivestream)
                        {
                            for($cnt = 0;$cnt < count($allLivestream) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allLivestream[$cnt]->getUsername();?></td>
                                <td><?php echo $allLivestream[$cnt]->getChannel();?></td>
                                <td><?php echo $allLivestream[$cnt]->getDate();?></td>
                                <td><?php echo $allLivestream[$cnt]->getTimeStart();?></td>
                                <td><?php echo $allLivestream[$cnt]->getTimeEnd();?></td>

                                <td>
                                    <form action="adminLivestreamEdit.php" method="POST" class="left-form">
                                        <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $allLivestream[$cnt]->getUid();?>">
                                            <u>Edit</u>
                                        </button>
                                    </form> 

                                    <form action="utilities/adminLivestreamDeleteFunction.php" method="POST" class="right-form">
                                        <button class="clean transparent-button red-link2" type="submit" name="item_uid" value="<?php echo $allLivestream[$cnt]->getUid();?>">
                                            <u>Delete</u>
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                            <?php
                            }
                        }
                    ?>   
                </tbody>
            </table>
		</div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>