<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bank.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$bankList = getBank($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="My Bank Details | MODERCK" />
<title>My Bank Details | MODERCK</title>


<?php include 'css.php'; ?>
</head>
<!-- PDF Page 62 -->
<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">My Bank Details</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">

	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding normal-min-height padding-top overflow">
		      
        <form action="utilities/userBankDetailsFunction.php" method="POST">
            	<div class="dual-input">
        			<p class="top-p">Bank Name</p>
                <select  class="line-input clean" id="bank_name" name="bank_name" required>
                
                <?php
                for ($cnt=0; $cnt <count($bankList) ; $cnt++)
                {
                ?>
                    <option value="<?php echo $bankList[$cnt]->getName(); ?>">
                        <?php echo $bankList[$cnt]->getName();?>
                    </option>
                <?php
                }
                ?>
            </select>
				</div>
                 
                <div class="dual-input second-dual-input">  
                    <p class="top-p">Account Holder Name</p>
                <input type="text" class="line-input clean" placeholder="Account Holder Name" id="acc_holder_name" name="acc_holder_name" required>
				</div> 
                <div class="clear"></div>   
            	<div class="dual-input">
        			<p class="top-p">NRIC No</p>
                <input type="text" class="line-input clean" placeholder="NRIC No" id="nric" name="nric" required>
				</div>  
                <div class="dual-input second-dual-input">  
                <p class="top-p">Account Number</p>
				<input type="text" class="line-input clean" placeholder="Account Number" id="acc_no" name="acc_no" required>
				</div> 
            <div class="clear"></div> 
             <p class="small-term white-text text-center">I have read and agreed to abide by The Company’s Terms of Use and Privacy Policy. By submitting this form, I hereby authorize The Company to store and transmit the above data for their own internal reference.</p>  
                <div class="text-center middle-div-width">
               
                <button class="clean yellow-btn edit-profile-width" name="submit">Submit</button> 
                </div>         
        </form>    

    </div>
</div>

<div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>