<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/PreOrderList.php';
// require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $products = getPreOrderList($conn, "WHERE status = 'Pending' ");
// $products = getOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
$products = getOrderList($conn, "WHERE user_uid = ? AND order_id = ? ",array("user_uid","order_id"),array($uid,$orderUid),"ss");

// $states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Checkout | MODERCK" />
<title>Checkout | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Checkout</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">
            <div class="width100 overflow-x">
                <table class="width100 tur-table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Product Name</th>
                            <th>Unit Price (RM)</th>
                            <th>Quantity</th>
                            <th>Subtotal (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($products)
                            {
                                for($cnt = 0;$cnt < count($products) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $products[$cnt]->getProductName();?></td>
                                        <td><?php echo $products[$cnt]->getOriginalPrice();?></td>
                                        <td><?php echo $products[$cnt]->getQuantity();?></td>
                                        <td><?php echo $products[$cnt]->getTotalPrice();?></td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>
        </div>

        <?php
        if($products)
        {
        $totalOrderAmount = 0;
        for ($cnt=0; $cnt <count($products) ; $cnt++)
        {
            $totalOrderAmount += $products[$cnt]->getTotalPrice();
            // echo "<br>";
            // echo '123';
        }
        }
        else
        {
            $totalOrderAmount = 0 ;
        }
        ?>

		<div class="clear"></div>

            <form method="POST" action="utilities/createOrderFunction.php">
                <p class="review-product-name" style="margin-top:20px;">Deliver to</p>
                <div class="width100 overflow-x">
                


                

                    <div class="dual-input">
                        <p class="input-top-p">Name : <!--<?php echo $totalOrderAmount;?>--></p>
                        <input class="line-input clean" type="text" id="insert_name" name="insert_name" placeholder="Name" value="<?php echo $userDetails[0]->getUsername();?>" required>
                    </div>

                    <div class="dual-input second-dual-input">
                        <p class="input-top-p">Phone No.</p>
                        <input class="line-input clean" type="text" id="insert_contact" name="insert_contact" placeholder="Phone No." value="<?php echo $userDetails[0]->getPhoneNo();?>" required> 
                    </div>

                    <div class="clear"></div>

                    <div class="width100">
                        <p class="input-top-p">Address</p>
                        <input class="line-input clean" type="text" id="insert_address" name="insert_address" value="<?php echo $userDetails[0]->getAddress();?>" placeholder="Address" required>   
                        <!-- <input class="line-input clean" type="text" id="insert_address" name="insert_address" placeholder="Address" required> -->
                    </div>

					<div class="clear"></div> 

                    <div class="dual-input">
                        <p class="input-top-p">Area</p>
                        <input class="line-input clean" type="text" placeholder="Area" value="<?php echo $userDetails[0]->getArea();?>" name="insert_area" id="insert_area" required> 
                        <!-- <input class="line-input clean" type="text" placeholder="Area" name="insert_area" id="insert_area" required>  -->
                    </div>      


                    <div class="dual-input second-dual-input">
                        <p class="input-top-p">Postal Code</p>
                        <input class="line-input clean" type="text" placeholder="Postal Code" value="<?php echo $userDetails[0]->getPostcode();?>" name="insert_code" id="insert_code" required>     
                        <!-- <input class="line-input clean" type="text" placeholder="Postal Code" name="insert_code" id="insert_code" required>    -->
                    </div> 

                    <div class="clear"></div> 

                    <div class="dual-input">
                        <p class="input-top-p">State</p>
                        <input class="line-input clean" type="text" placeholder="State" value="<?php echo $userDetails[0]->getState();?>" name="insert_state" id="insert_state" required>   

                    </div>   

                    <div class="clear"></div> 

                    <input type="hidden" id="order_uid" name="order_uid" value="<?php echo $orderUid ?>" readonly>
                    <input type="hidden" id="subtotal" name="subtotal" value="<?php echo $totalOrderAmount;?>" readonly> 

                    <div class="clear"></div>  

                </div>

				<div class="width100 text-center margin-top30">
                    <button class="green-button white-text clean2 width100" type="submit" name="submit" style="margin-bottom:50px;">Continue To Payment</button>
				</div>

            </form>
    
    </div>
</div>


<?php include 'js.php'; ?>

</body>
</html>