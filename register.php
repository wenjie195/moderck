<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/" />
<link rel="canonical" href="https://agentpnchc.com/" /> -->
<meta property="og:title" content="REGISTER | MODERCK" />
<title>REGISTER | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding  min-height100 padding-top login-bg padding-top-bottom">
    <div class="overflow width100 text-center">
    	<img src="img/logo.png" class="logo" alt="Moderck" title="Moderck">
	</div>

    <h1 class="h1 red-text text-center login-h1"><br><b>Create Your Login Profile</b></h1>

    <div class="login-div margin-auto">
        <form action="utilities/registerFunction.php" method="POST">

        <?php
        // Program to display URL of current page.
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        $link = "https";
        else
        $link = "http";

        // Here append the common URL characters.
        $link .= "://";

        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];

        // Append the requested resource location to the URL
        $link .= $_SERVER['REQUEST_URI'];

        // Print the link
        // echo $link;
        ?>

        <?php
        $str1 = $link;
        // $referrerUidLink = str_replace( 'http://localhost/poppifx/register.php?referrerUID=', '', $str1);
        if(isset($_GET['referrerUID'])){
          $referrerUidLink = $_GET['referrerUID'];
        }else {
          $referrerUidLink = "";
        }
        // echo $referrerUidLink;
        ?>
        
            <div class="fake-input-div">
                <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Username" id="username" name="username" onChange="javascript:this.value=this.value.toLowerCase();" required>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="email" class="input-css clean icon-input dark-tur-text2" placeholder="Email" id="email" name="email" required>
            </div>

            <!-- <div class="fake-input-div before-forgot"> -->
            <div class="fake-input-div">
                <!-- <img src="img/password.png" class="input-icon"> -->
                <input type="password" class="input-css clean password-input dark-tur-text2" placeholder="Password" id="password" name="password" required>
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionA()" alt="Password" title="Password">
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="password" class="input-css clean password-input dark-tur-text2" placeholder="Confirm Password" id="retype_password" name="retype_password" required>
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionB()" alt="Password" title="Password">
            </div>

            <div class="clear"></div>

            <input class="input-css clean icon-input dark-tur-text2" type="hidden" value="<?php echo $referrerUidLink ?>" id="upline_uid" name="upline_uid" readonly>

            <button class="clean white-button ow-red-bg white-text" name="submit">
                Register
            </button>

            <div class="clear"></div>
        </form>
    </div>
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Wrong Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunctionA()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>