<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allLivestream = getLivestream($conn);
$allProduct = getProduct($conn, " WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="All Product | MODERCK" />
<title>All Product | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">All Product</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div padding-bottom30">
    		<div class="text-center middle-div-width">
            	<a href="adminProductAdd.php"><div  class="clean white-button ow-red-bg white-text smaller-button">Add</div></a> 
            </div> 
             <div class="width100 scroll-div">
            <table class="width100 gold-table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>PRODUCT NAME</th>
                        <th>PRODUCT CODE</th>
                        <th>SELLING PRICE (RM)</th>
                        <th>PV</th>
                        <th>RP</th>
                        <th>STATUS</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($allProduct)
                        {
                            for($cnt = 0;$cnt < count($allProduct) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allProduct[$cnt]->getName();?></td>
                                <td><?php echo $allProduct[$cnt]->getCode();?></td>
                                <td><?php echo $allProduct[$cnt]->getPrice();?></td>
                                <td><?php echo $allProduct[$cnt]->getProductValue();?></td>
                                <td><?php echo $allProduct[$cnt]->getRedemptionPoint();?></td>
                                <td><?php echo $allProduct[$cnt]->getStatus();?></td>

                                <td>
                                    <form action="adminProductEdit.php" method="POST" class="left-form">
                                        <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $allProduct[$cnt]->getUid();?>">
                                            <u>Edit</u>
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                            <?php
                            }
                        }
                    ?>   
                </tbody>
            </table>
		</div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>