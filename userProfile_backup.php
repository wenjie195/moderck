<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$uplineUid = $userRHDetails[0]->getReferrerId();

$uplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineUid),"s");
$uplineUsername = $uplineDetails[0]->getUsername();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Profile | MODERCK" />
<title>Profile | MODERCK</title>


<?php include 'css.php'; ?>
</head>
<!-- pdf pg 54 -->
<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Profile</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">

	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding normal-min-height padding-top overflow ow-700-center">
		      <p class="text-center acc-p gold-text">Account Type : <?php echo $userData->getUserType();?></p>
        <form action="utilities/editProfileFunction.php" method="POST">

                          


		<div class="dual-input">
        	<p class="top-p">Full Name</p>
            <input type="text" class="line-input clean" placeholder="Account Type" value="<?php echo $userData->getFullname();?>" readonly>

        </div>  
        <div class="dual-input second-dual-input">  
        	<p class="top-p">Registration Date</p>
            <!-- <input type="text" class="rec-input clean" placeholder="Registration Date" value="<?php //echo $userData->getDateCreated();?>" readonly> -->
            <input type="text" class="line-input clean" placeholder="Registration Date" value="<?php echo $date = date("Y-m-d",strtotime($userData->getDateUpdated()));?>" readonly>
		</div>
         <div class="clear"></div>  
		<div class="dual-input">
        	<p class="top-p">Promoter Username</p>

            <input type="text" class="line-input clean" placeholder="Promoter Username" value="<?php echo $uplineUsername;?>" readonly>
		</div>
        <div class="dual-input second-dual-input">  
        	<p class="top-p">Username</p>            
            <input type="text" class="line-input clean" placeholder="Username" value="<?php echo $userData->getUsername();?>" id="update_username" name="update_username" required>
		</div>
        <div class="clear"></div>     
		<div class="dual-input">
        	<p class="top-p">Email</p>
            <input type="email" class="line-input clean" placeholder="Email" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email" required>
		</div>
        <div class="dual-input second-dual-input">  
        	<p class="top-p">Password</p>             

            <input type="text" class="line-input clean" placeholder="Password" value="Password" id="password" name="password" required>
		</div>
        <div class="clear"></div>     
		<div class="dual-input">
        	<p class="top-p">Contact Number</p>
           
            
            <input type="text" class="line-input clean" placeholder="Contact Number" value="<?php echo $userData->getPhoneNo();?>" id="update_phone" name="update_phone">
		</div>
        <div class="dual-input second-dual-input">  
        	<p class="top-p">Corresponding Address</p>      
            

            <input type="text" class="line-input clean" placeholder="Corresponding Address" value="Corresponding Address" id="update_address" name="update_address">
		</div>
        <div class="clear"></div>     
		<div class="dual-input">
        	<p class="top-p">City</p>            
            <input type="text" class="line-input clean" placeholder="City" value="City" id="update_city" name="update_city">
		</div>
        <div class="dual-input second-dual-input">  
        	<p class="top-p">State</p>                 
            <input type="text" class="line-input clean" placeholder="State" value="State" id="update_state" name="update_state">
		</div>
        <div class="clear"></div>     
		<div class="dual-input">
        	<p class="top-p">Postcode</p>             
            <input type="text" class="line-input clean" placeholder="Postcode" value="Postcode" id="update_postcode" name="update_postcode">
		</div>
        <div class="dual-input second-dual-input">  
        	<p class="top-p">Country</p>             
            <input type="text" class="line-input clean" placeholder="Country" value="Country" id="update_country" name="update_country">
		</div>
        <div class="clear"></div>     
		<div class="dual-input">
        	<p class="top-p">Bank Name</p>   
          
            <input type="text" class="line-input clean" placeholder="Bank Name" value="<?php echo $userData->getBankName();?>" id="update_bank" name="update_bank">
        </div>  
		<div class="dual-input second-dual-input">
        	<p class="top-p">NRIC No</p> 
          

            <input type="text" class="line-input clean" placeholder="NRIC No" value="<?php echo $userData->getIcno();?>" id="update_icno" name="update_icno" required>
		</div>
        <div class="clear"></div>     
		<div class="dual-input"> 
        	<p class="top-p">Account Holder Name</p>   
           

            <input type="text" class="line-input clean" placeholder="Account Holder Name" value="<?php echo $userData->getBankAccName();?>" id="update_bank_holder" name="update_bank_holder">    
        </div>  
		<div class="dual-input second-dual-input">
          <p class="top-p">Account Number</p>   
            
            <input type="text" class="line-input clean" placeholder="Account Number" value="<?php echo $userData->getBankAccNumber();?>" id="update_bank_account" name="update_bank_account">           
		</div>
        <div class="clear"></div>            
           
			<div class="text-center middle-div-width">
            	<button class="clean white-button ow-red-bg white-text" name="submit">Save Changes</button> 
            </div>         
        </form> 
        <!-- <form>
        	<div class="text-center middle-div-width">
        		<button class="transparent-btn dark-tur-link">Delete Account</button>
             </div>
        </form>    -->
    </div>
</div>

<div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>