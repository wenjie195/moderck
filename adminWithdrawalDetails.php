<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Withdrawal Details | MODERCK" />
<title>Withdrawal Details | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Withdrawal Details</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 scroll-div">

                <?php
                if(isset($_POST['item_uid']))
                {
                $conn = connDB();
                $itemInfo = getWithdrawal($conn,"WHERE withdrawal_uid = ? ", array("withdrawal_uid") ,array($_POST['item_uid']),"s");
                ?>

                    <div class="dual-input">
                        <p class="top-p">BANK NAME</p>
                        <input type="text" class="line-input clean" value="<?php echo $itemInfo[0]->getBankName();?>" readonly>
                    </div>
                        
                    <div class="dual-input second-dual-input">  
                        <p class="top-p">BANK ACCOUNT HOLDER</p>
                        <input type="text" class="line-input clean" value="<?php echo $itemInfo[0]->getBankAccHolder();?>" readonly>
                    </div> 

                    <div class="clear"></div>   

                    <div class="dual-input">
                        <p class="top-p">BANK ACCOUNT NUMBER</p>
                        <input type="text" class="line-input clean" value="<?php echo $itemInfo[0]->getBankAccNo();?>" readonly>
                    </div>
                        
                    <div class="dual-input second-dual-input">  
                        <p class="top-p">AMOUNT</p>
                        <input type="text" class="line-input clean" value="<?php echo $itemInfo[0]->getAmount();?>" readonly>
                    </div> 
                        
                    <div class="clear"></div>   

                    <?php 
                        $status = $itemInfo[0]->getStatus();
                        if($status == 'PENDING')
                        {
                        ?>

                        <!-- <form method="POST" action="utilities/adminUpdateWithdrawalFunction.php" enctype="multipart/form-data"> -->
                        <form action="utilities/adminUpdateWithdrawalFunction.php" method="POST">

                            <div class="dual-input">
                                <p class="top-p">Payment Method</p>
                                <input type="text" class="line-input clean" placeholder="Payment Method" id="payment_method" name="payment_method">
                            </div>
                                
                            <div class="dual-input second-dual-input">  
                                <p class="top-p">Reference</p>
                                <input type="text" class="line-input clean" placeholder="Reference" id="reference" name="reference">
                            </div>

                            <div class="clear"></div>   

                            <div class="dual-input">
                                <p class="top-p">Transaction Date</p>
                                <input type="date" class="line-input clean" placeholder="Transaction Date" id="transaction_date" name="transaction_date">
                            </div>

                            <!-- <div class="dual-input second-dual-input">  
                                <p class="top-p">Upload Receipt</p>
                                <input class="hidden-input" type="file" name="file" />
                            </div> -->

                            <div class="clear"></div>   

                            <input type="text" class="line-input clean" value="<?php echo $_POST['item_uid'];?>" id="item_uid" name="item_uid" readonly>

                            <div class="text-center middle-div-width padding-bottom30">
                                <button class="clean white-button ow-red-bg white-text" name="submit">Approve</button>
                            </div> 

                            </form>

                            <form action="utilities/adminUpdateWithdrawalRejectedFunction.php" method="POST">

                            <div class="dual-input">
                                <p class="top-p">Reject Reason</p>
                                <input type="text" class="line-input clean" placeholder="Reject Reason" id="reject_season" name="reject_season">
                            </div>

                            <input type="hidden" class="line-input clean" value="<?php echo $_POST['item_uid'];?>" id="item_uid" name="item_uid" readonly>

                            <div class="text-center middle-div-width padding-bottom30">
                                <button class="clean white-button ow-red-bg white-text" name="submit">Reject</button>
                            </div> 

                        </form>

                        <?php
                        }
                        elseif($status == 'APPROVED')
                        {
                        ?>
                        
                            <div class="dual-input">
                                <p class="top-p">Payment Method</p>
                                <input type="text" class="line-input clean" value="<?php echo $itemInfo[0]->getMethod();?>" readonly>
                            </div>
                                
                            <div class="dual-input second-dual-input">  
                                <p class="top-p">Reference</p>
                                <input type="text" class="line-input clean" value="<?php echo $itemInfo[0]->getReceipt();?>" readonly>
                            </div>

                            <div class="clear"></div>   

                            <div class="dual-input">
                                <p class="top-p">Transaction Date</p>
                                <input type="date" class="line-input clean" value="<?php echo $itemInfo[0]->getNote();?>" readonly>
                            </div>

                        <?php
                        }
                        elseif($status == 'REJECTED')
                        {
                        ?>
                        
                            <div class="dual-input">
                                <p class="top-p">Reject Reason</p>
                                <input type="text" class="line-input clean" value="<?php echo $itemInfo[0]->getReason();?>" readonly>
                            </div>

                        <?php
                        }
                    ?>

                <?php
                }
                ?>

            </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>