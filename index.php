<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/" />
<link rel="canonical" href="https://agentpnchc.com/" /> -->
<meta property="og:title" content="MODERCK" />
<title>MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding  min-height100 padding-top login-bg padding-top-bottom">
    <div class="overflow width100 text-center">
    	<img src="img/logo.png" class="logo" alt="Moderck" title="Moderck">
	</div>
    <div class="login-div margin-auto">
        <form action="utilities/loginFunction.php" method="POST">
            <div class="fake-input-div">
                <!-- <img src="img/user.png" class="input-icon"> -->
                <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Username" id="username" name="username" required>
                <!-- <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Email (Username)" id="username" name="username" required> -->
                <!-- <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Email (Username)" id="email" name="email" required> -->
            </div>
            <div class="clear"></div>
            <div class="fake-input-div before-forgot">
                <!-- <img src="img/password.png" class="input-icon"> -->
                <input type="password" class="input-css clean password-input dark-tur-text2" placeholder="Password" id="password" name="password" required>
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionC()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
            </div>
            <div class="clear"></div>
            <!-- <p class="forgot-p"><a href="forgot.php" class="dark-tur-link forgot-a">Forget Password</a></p> -->
            <p class="forgot-p"></p>
            <div class="clear"></div>
            <button class="clean white-button ow-red-bg white-text" name="loginButton">
                Login
            </button>
            <div class="clear"></div>
        </form>
    </div>
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Wrong Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>