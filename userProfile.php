<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$uplineUid = $userRHDetails[0]->getReferrerId();

$uplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineUid),"s");
$uplineUsername = $uplineDetails[0]->getUsername();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Profile | MODERCK" />
<title>Profile | MODERCK</title>


<?php include 'css.php'; ?>
</head>
<!-- pdf pg 54 -->
<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Profile</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">

	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding normal-min-height padding-top overflow ow-700-center">
        
        <p class="text-center acc-p gold-text">Account Type : <?php echo $userData->getRank();?></p>

        <form action="utilities/userEditProfileFunction.php" method="POST">

            <div class="dual-input">
                <p class="top-p">Username</p>            
                <input type="text" class="line-input clean" placeholder="Username" value="<?php echo $userData->getUsername();?>" id="update_username" name="update_username" required>
            </div>
            <div class="dual-input second-dual-input">  
                <p class="top-p">Full Name</p>
                <input type="text" class="line-input clean" placeholder="Fullname" value="<?php echo $userData->getFullname();?>" id="update_fullname" name="update_fullname">
            </div>  

            <div class="clear"></div>  

            <div class="dual-input">
                <p class="top-p">Promoter Username</p>
                <input type="text" class="line-input clean" value="<?php echo $uplineUsername;?>" readonly>
            </div>

            <div class="dual-input second-dual-input">  
                <p class="top-p">Registration Date</p>
                <input type="text" class="line-input clean" value="<?php echo $date = date("Y-m-d",strtotime($userData->getDateUpdated()));?>" readonly>
            </div>

            <div class="clear"></div>   

            <div class="dual-input">
                <p class="top-p">NRIC No</p> 
                <input type="text" class="line-input clean" placeholder="NRIC No" value="<?php echo $userData->getIcno();?>" id="update_icno" name="update_icno" required>
            </div>
            <div class="dual-input second-dual-input">
                <p class="top-p">Email</p>
                <input type="email" class="line-input clean" placeholder="Email" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email" required>
            </div>

            <div class="clear"></div>     
            
            <div class="dual-input">
                <p class="top-p">Contact Number</p>
                <input type="text" class="line-input clean" placeholder="Contact Number" value="<?php echo $userData->getPhoneNo();?>" id="update_phone" name="update_phone">
            </div>

            <div class="clear"></div>     

            <div class="dual-input">
                <p class="top-p">Bank Name</p>   
                <input type="text" class="line-input clean" placeholder="Bank Name" value="<?php echo $userData->getBankName();?>" id="update_bank" name="update_bank">
            </div>  
            <div class="dual-input second-dual-input">
                <p class="top-p">Account Holder Name</p>   
                <input type="text" class="line-input clean" placeholder="Account Holder Name" value="<?php echo $userData->getBankAccName();?>" id="update_bank_holder" name="update_bank_holder">    
            </div>  

            <div class="clear"></div>     

            <div class="dual-input">
            <p class="top-p">Account Number</p>   
                <input type="text" class="line-input clean" placeholder="Account Number" value="<?php echo $userData->getBankAccNumber();?>" id="update_bank_account" name="update_bank_account">           
            </div>

            <div class="clear"></div>            
            
            <div class="text-center middle-div-width">
                <button class="clean white-button ow-red-bg white-text" name="submit">Save Changes</button> 
            </div>         

        </form> 

        <div class="clear"></div>     

        <h1 class="top-title brown-text padding-top30">Edit Password</h1>

        <div class="clear"></div>     

    <form action="utilities/userEditPasswordFunction.php" method="POST">

        <div class="dual-input">
            <p class="top-p">Current Passowrd</p>   
            <input type="password" class="line-input clean" placeholder="Current Passowrd" id="current_password" name="current_password">           
            <img src="img/view.png" class="input-icon view-icon opacity-hover" onclick="myFunctionA()" alt="View Password" title="View Password">    
        </div>

            <div class="clear"></div>    

        <div class="dual-input">
            <p class="top-p">New Passowrd</p>   
            <input type="password" class="line-input clean" placeholder="New Passowrd" id="new_password" name="new_password">           
            <img src="img/view.png" class="input-icon view-icon opacity-hover" onclick="myFunctionB()" alt="View Password" title="View Password">    
        </div>
        <div class="dual-input second-dual-input">
            <p class="top-p">Retype New Passowrd</p>   
            <input type="password" class="line-input clean" placeholder="Retype New Passowrd" id="retype_new_password" name="retype_new_password">           
            <img src="img/view.png" class="input-icon view-icon opacity-hover" onclick="myFunctionC()" alt="View Password" title="View Password">    
        </div>

        <div class="text-center middle-div-width">
            <button class="clean white-button ow-red-bg white-text" name="submit">Submit</button> 
        </div>     

    </form>

    </div>
</div>

<div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Password Updated !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Fail Tp Updated Profile";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "ERROR";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Fail Tp Updated Password";
        }

        elseif($_GET['type'] == 6)
        {
            $messageType = "New Password is Different With Retype New Password !";
        }
        elseif($_GET['type'] == 7)
        {
            $messageType = "Password Length Must Be More Than 5 !";
        }
        elseif($_GET['type'] == 8)
        {
            $messageType = "Incorrect Current Password !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunctionA()
{
    var x = document.getElementById("current_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>