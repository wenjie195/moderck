<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="MEMBER PROFILE | MODERCK" />
<title>MEMBER PROFILE | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">PROFILE</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
        <div class="width100 same-padding min-height100 padding-top overflow overflow-x">

                <!-- <div class="text-center middle-div-width padding-bottom30">
                    <a href="adminMemberOverview.php"><div class="clean white-button ow-red-bg white-text smaller-button">Go Back</div></a> 
                </div>  -->
                
                <?php
                if(isset($_POST['item_uid']))
                {
                $conn = connDB();
                $itemDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['item_uid']),"s");
                $addressDetails = getAddress($conn,"WHERE user_uid = ? AND default_ship = 'Yes' ", array("user_uid") ,array($_POST['item_uid']),"s");
                ?>
                
                    <div class="dual-input">
                        <p class="top-p">Registration Date</p>
                        <input type="text" class="line-input clean ow-border0" value="<?php echo date("Y-m-d",strtotime($itemDetails[0]->getDateCreated()));?>" readonly>
                    </div>

                    <div class="dual-input second-dual-input"> 
                        <p class="top-p">Rank</p>
                        <input type="text" class="line-input clean ow-border0" value="<?php echo $itemDetails[0]->getRank();?>" readonly>
                    </div>

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">Username</p>
                        <input type="text" class="line-input clean ow-border0" value="<?php echo $itemDetails[0]->getUsername();?>" readonly>
                    </div>

                    <div class="dual-input second-dual-input"> 
                        <p class="top-p">Email</p>
                        <input type="text" class="line-input clean ow-border0" value="<?php echo $itemDetails[0]->getEmail();?>" readonly>
                    </div>

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">Contact</p>
                        <input type="text" class="line-input clean ow-border0" value="<?php echo $itemDetails[0]->getPhoneNo();?>" readonly>
                    </div>

                    <div class="clear"></div>    
                    

                    <?php
                    if(!$addressDetails)
                    {
                    ?>
                        <h1 class="top-title brown-text">Address Haven't Set</h1>
                    <?php
                    }
                    else
                    {
                    ?>

                        <h1 class="top-title brown-text">Address</h1>

                        <div class="dual-input">
                            <p class="top-p">House No. & Street</p>
                            <input type="text" class="line-input clean ow-border0" placeholder="AA" value="<?php echo $addressDetails[0]->getHouseRoad();?>" readonly>
                        </div>

                        <div class="dual-input second-dual-input"> 
                            <p class="top-p">Postcode</p>
                            <input type="text" class="line-input clean ow-border0" placeholder="DD" value="<?php echo $addressDetails[0]->getPostcode();?>" readonly>
                        </div>

                        <div class="clear"></div>    

                        <div class="dual-input">
                            <p class="top-p">City</p>
                            <input type="text" class="line-input clean ow-border0"  placeholder="BB" value="<?php echo $addressDetails[0]->getCity();?>" readonly>
                        </div>

                        <div class="dual-input second-dual-input"> 
                            <p class="top-p">State</p>
                            <input type="text" class="line-input clean ow-border0" placeholder="CC" value="<?php echo $addressDetails[0]->getState();?>" readonly>
                        </div>

                        <div class="clear"></div>      

                        <div class="dual-input">
                            <p class="top-p">Country</p>
                            <input type="text" class="line-input clean ow-border0" placeholder="EE" value="<?php echo $addressDetails[0]->getCountry();?>" readonly>
                        </div>

                    <?php
                    }
                    ?>
                
                <?php
                }
                ?>

            <div class="clear"></div>

            <div class="text-center middle-div-width padding-bottom30">
                <a href="adminMemberOverview.php"><div class="clean white-button ow-red-bg white-text smaller-button">Go Back</div></a> 
            </div> 

        </div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>