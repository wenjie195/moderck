<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Upgrading.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allUpgrade = getUpgrading($conn, " ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="MEMBER UPGRADING | MODERCK" />
<title>MEMBER UPGRADING | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">MEMBER UPGRADING</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div padding-bottom30">

            <table class="width100 gold-table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>USERNAME</th>
                        <th>IP ADDRESS</th>
                        <th>UPGRADE BY</th>
                        <th>DATE</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                        if($allUpgrade)
                        {
                            for($cnt = 0;$cnt < count($allUpgrade) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>

                                <td>
                                    <?php 
                                        $userUid = $allUpgrade[$cnt]->getUserUid();
                                        $conn = connDB();
                                        $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");   
                                        echo $userDetails[0]->getUsername();
                                        $conn->close();
                                    ?>
                                </td>

                                <td><?php echo $allUpgrade[$cnt]->getIpAddress();?></td>

                                <td>
                                    <?php 
                                        $editBy = $allUpgrade[$cnt]->getEditBy();
                                        $conn = connDB();
                                        $editorDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($editBy),"s");   
                                        echo $editorDetails[0]->getUsername();
                                        $conn->close();
                                    ?>
                                </td>   

                                <td><?php echo $allUpgrade[$cnt]->getDateCreated();?></td>                               
                            <?php
                            }
                        }
                    ?>  
                    <tr> 
                </tbody>
            </table>

    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>